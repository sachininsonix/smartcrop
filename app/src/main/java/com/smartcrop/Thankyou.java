package com.smartcrop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by insonix on 19/1/16.
 */
public class Thankyou extends Activity {
    ImageView back,setting;
    String subcat_id,city_id,cat_name,sub_name;
    Button homescre,buyers,sellers,mandi;
    TextView smartcrop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thanks);
        mandi=(Button)findViewById(R.id.mandi);
        buyers=(Button)findViewById(R.id.buyers);
        sellers=(Button)findViewById(R.id.sellers);
        homescre=(Button)findViewById(R.id.homescre);
        smartcrop=(TextView)findViewById(R.id.smartcrop);
        try {
            subcat_id = getIntent().getExtras().getString("subcat_id");
            cat_name = getIntent().getExtras().getString("catname");
            sub_name = getIntent().getExtras().getString("sub_cat_name");
            Log.d("subcat_id:", "subcat_id" + subcat_id);
        }catch(Exception e){

        }
        homescre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Thankyou.this,HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });
        sellers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Thankyou.this,SellingList.class);
                intent.putExtra("subcat_id",subcat_id);
                intent.putExtra("catname",cat_name);
                intent.putExtra("sub_cat_name",sub_name);
                startActivity(intent);
                finish();
            }
        });
        buyers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Thankyou.this,BuyList.class);
                intent.putExtra("subcat_id",subcat_id);
                intent.putExtra("catname",cat_name);
                intent.putExtra("sub_cat_name",sub_name);
                startActivity(intent);
                finish();
            }
        });
        mandi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Thankyou.this,MandiRate.class);
                startActivity(intent);
                finish();
            }
        });
        smartcrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Thankyou.this, HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });


//        back=(ImageView)findViewById(R.id.menu);
//        setting=(ImageView)findViewById(R.id.setting);
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(BuySellActivity.this,HomeCategory.class);
//                startActivity(intent);
//                finish();
//            }
//        });
//        setting.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(BuySellActivity.this,SettingScrren.class);
//                startActivity(intent);
//                finish();
//            }
//        });
    }
}
