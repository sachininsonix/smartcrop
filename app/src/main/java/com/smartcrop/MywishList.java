package com.smartcrop;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by insonix on 16/12/15.
 */
public class MywishList extends Activity {
    ImageView back,setting,plus;
    ListView wishlist;
    TextView smartcrop;
    //
    SellListAdapter sellListAdapter;
    String url1,lang,url2,id;
    ProgressDialog progressDialog;
    SharedPreferences preferences;

    ArrayList<HashMap<String,String >> addme;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mywish);
        wishlist=(ListView)findViewById(R.id.wishlist);
        plus=(ImageView)findViewById(R.id.plus);

        back=(ImageView)findViewById(R.id.menu);
        smartcrop=(TextView)findViewById(R.id.smartcrop);
        setting=(ImageView)findViewById(R.id.setting);
        progressDialog=new ProgressDialog(MywishList.this);
        addme=new ArrayList<>();
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MywishList.this,SettingScrren.class);
                startActivity(intent);
                finish();
            }
        });
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MywishList.this,Addwish.class);
                startActivity(intent);
                finish();
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MywishList.this,SettingScrren.class);
                startActivity(intent);
//                finish();
            }

        });
        smartcrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MywishList.this, HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });
        if(preferences.getString("on","").equals("1")){
            lang="1";
//
        }else if(preferences.getString("on","").equals("2")){
            lang="2";
//
        }else{
            lang="0";
        }
        wishlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MywishList.this, EditWishList.class);
                intent.putExtra("hashme", addme);
                intent.putExtra("position", position);
                startActivity(intent);
                finish();
            }
        });


        SellList();
    }
    public void SellList(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url1="http://52.35.22.61/smart_crop/get_wishlist.php?user_id="+preferences.getString("uid","");
        Log.d("url1","url1"+url1);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("Status");
                    if(status.equalsIgnoreCase("OK")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("wish");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject City = jsonArray.getJSONObject(i);

                            String id = City.getString("ID");

                            String min = City.getString("min");
                            String max = City.getString("max");
                            String CatName = City.getString("CatName");
                            String cat_id = City.getString("cat_id");
                            String state = City.getString("state");


                            HashMap<String, String> hashMap = new HashMap<>();

                            hashMap.put("id", id);
                            hashMap.put("CatName", CatName);
                            hashMap.put("cat_id", cat_id);
                            hashMap.put("min", min);
                            hashMap.put("max", max);
                            hashMap.put("state", state);



                            addme.add(hashMap);
                        }
                        sellListAdapter=new SellListAdapter(MywishList.this, addme);
                        wishlist.setAdapter(sellListAdapter);
                    }else{
                        new ShowMsg().createDialog(MywishList.this,"No Wishlist");
                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class SellListAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> addme;
        public SellListAdapter(Activity activity,ArrayList<HashMap<String,String>>addme){
            this.activity=activity;
            this.addme=addme;
        }
        @Override
        public int getCount() {
            return addme.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.wish_item, null);

            TextView wish=(TextView)convertView.findViewById(R.id.wish);
            ImageView cross=(ImageView)convertView.findViewById(R.id.cross_1);
            ImageView edit=(ImageView)findViewById(R.id.edit);
//           cross.setTag(position);
            cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    id=addme.get(position).get("id");
                    AlertDialog.Builder alert = new AlertDialog.Builder(MywishList.this);
                    alert.setTitle("Are you sure to delete this item from your wishlist?");


                    alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            addme.remove(position);
                            sellListAdapter.notifyDataSetChanged();
                            Deletewish();


                        }
                    });

                    alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            // Canceled.
                            dialog.dismiss();

                        }
                    });


                    alert.show();

                }
            });
//            edit.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    id=addme.get(position).get("id");
//                    Intent intent = new Intent(MywishList.this, EditWishList.class);
//                    intent.putExtra("hashme", addme);
//                    intent.putExtra("position", position);
//                    startActivity(intent);
//                    finish();
//                }
//            });



            wish.setText(addme.get(position).get("CatName"));


//

            return convertView;
        }

    }
    public void Deletewish(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url2="http://52.35.22.61/smart_crop/delete_wishlist.php?wishlist_id="+id;
        Log.d("url2","url2"+url2);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url2, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("Status");
                    if(status.equalsIgnoreCase("OK")) {
//                        wishlist.setAdapter(sellListAdapter);
                        Toast.makeText(MywishList.this, "Successfully deleted", Toast.LENGTH_SHORT).show();


                    }else{
                        new ShowMsg().createDialog(MywishList.this,"Not deleted");
                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(MywishList.this, SettingScrren.class);
//        startActivity(intent);
        finish();
    }

}
