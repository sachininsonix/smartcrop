package com.smartcrop;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

/**
 * Created by insonix on 17/11/15.
 */
public class DeleteAccount extends Activity {
    Button del;
    EditText phn_num;
    TextView county_code,smartcrop;
    TelephonyManager telephonyManager;
    String county_cod,url;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    ProgressDialog progressDialog;
    ImageView back,setting;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.del_account);
        progressDialog=new ProgressDialog(DeleteAccount.this);
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor=preferences.edit();
        del=(Button)findViewById(R.id.dele);
        phn_num=(EditText)findViewById(R.id.phn_num);
        county_code=(TextView)findViewById(R.id.county_code);
        smartcrop=(TextView)findViewById(R.id.smartcrop);


        telephonyManager=(TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        county_cod=telephonyManager.getSimCountryIso();
        county_code.setText(county_cod);
        back=(ImageView)findViewById(R.id.menu);
        setting=(ImageView)findViewById(R.id.setting);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DeleteAccount.this,SettingScrren.class);
                startActivity(intent);
                finish();
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DeleteAccount.this, SettingScrren.class);
                startActivity(intent);
//                finish();
            }
        });
        smartcrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DeleteAccount.this,HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });
        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(DeleteAccount.this);
                alert.setTitle("Are you sure to delete your account");
                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // validate = input.getText().toString();
                        DeleteAccountTask();
                        //new RegisterTask().execute();

                    }
                });

                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                        dialog.dismiss();

                    }
                });
                alert.show();

            }
        });
    }
    public void DeleteAccountTask(){

        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url="http://52.35.22.61/smart_crop/user_deactivate.php?id="+preferences.getString("uid","")+"&phone_number="+phn_num.getText().toString();

        Log.d("url1111:", "url" + url);
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try {
                    editor.clear();
                    editor.commit();
                    Intent intent=new Intent(DeleteAccount.this,Phone_numberSCreen.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
//                    String name = jsonObject.getString("name");
//                    String quantity = jsonObject.getString("quantity");
//                    String price = jsonObject.getString("price");
//                    String quantity = jsonObject.getString("quantity");
                    Toast.makeText(DeleteAccount.this, "Successfully Deactive", Toast.LENGTH_SHORT).show();

                }catch (Exception e){
                    Log.d("eeeee:", "eeeee" + e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:", "volleyError" + volleyError);
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(DeleteAccount.this, SettingScrren.class);
        startActivity(intent);
        finish();
    }
}

