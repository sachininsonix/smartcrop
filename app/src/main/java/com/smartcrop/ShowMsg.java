package com.smartcrop;



import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

public class ShowMsg {

	public void createToast(final Activity splash, final String string) {
		// TODO Auto-generated method stub
		
	
		
		splash.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				Toast.makeText(splash,string, Toast.LENGTH_SHORT).show();
				
			}
		});
		
	}
	
	
	

	public void createDialog(final Activity splash, final String string) {
		// TODO Auto-generated method stub
		
	
		
		splash.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				
				
				
				 Builder dialog = new Builder(splash);
				 dialog.setTitle("SmartCrop");
				 dialog.setMessage(string);
				 dialog.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if(string.contains("Your account is not verified")){
//							Intent i=new Intent(splash,VerifyCode.class);
//							splash.startActivity(i);
							
						}
						dialog.dismiss();
					}
				});
				 
				// dialog.show();
				 AlertDialog al= dialog.show();
				 TextView titleView = (TextView)al.findViewById(splash.getResources().getIdentifier("alertTitle", "id", "android"));
				 if (titleView != null) {
					    titleView.setGravity(Gravity.CENTER);
					}
				 TextView messageView = (TextView)al.findViewById(android.R.id.message);
				 messageView.setGravity(Gravity.CENTER);
				
			}
		});
		
	}
	
	
//	public void createDialogpdf(final Activity splash, final String string) {
//		// TODO Auto-generated method stub
//
//
//
//		splash.runOnUiThread(new Runnable() {
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//
//
//
//
//				 Builder dialog = new Builder(splash);
//				 dialog.setTitle("eCPD");
//				 dialog.setMessage(string);
//				 dialog.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
//
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						// TODO Auto-generated method stub
//
//							Intent i=new Intent(splash,Settings.class);
//							splash.startActivity(i);
//
//
////						dialog.dismiss();
//					}
//				});
//
//				// dialog.show();
//				 AlertDialog al= dialog.show();
//				 TextView titleView = (TextView)al.findViewById(splash.getResources().getIdentifier("alertTitle", "id", "android"));
//				 if (titleView != null) {
//					    titleView.setGravity(Gravity.CENTER);
//					}
//				 TextView messageView = (TextView)al.findViewById(android.R.id.message);
//				 messageView.setGravity(Gravity.CENTER);
//
//			}
//		});
//
//	}
//
//
//
//
	public void createDialogSignUp(final Activity splash, final String string) {
		// TODO Auto-generated method stub



		splash.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub




				 Builder dialog = new Builder(splash);

				 dialog.setTitle("SmartCrop");
				 dialog.setMessage(string);

				// dialog.
				 dialog.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					//	splash.finish();

						splash.startActivity(new Intent(splash, Phone_numberSCreen.class));
						splash.finish();

					}
				});

				AlertDialog al= dialog.show();
				 TextView titleView = (TextView)al.findViewById(splash.getResources().getIdentifier("alertTitle", "id", "android"));
				 if (titleView != null) {
					    titleView.setGravity(Gravity.CENTER);
					}
				 TextView messageView = (TextView)al.findViewById(android.R.id.message);
				 messageView.setGravity(Gravity.CENTER);
			}
		});

	}
//	public void createDialogSign(final Activity splash, final String string) {
//		// TODO Auto-generated method stub
//
//
//
//		splash.runOnUiThread(new Runnable() {
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//
//
//
//
//				 Builder dialog = new Builder(splash);
//
//				 dialog.setTitle("SsamTalk");
//				 dialog.setMessage(string);
//
//				// dialog.
//				 dialog.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
//
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						// TODO Auto-generated method stub
//						dialog.dismiss();
//					//	splash.finish();
//
//						splash.startActivity(new Intent(splash, LoginActivity.class));
//						splash.finish();
//
//					}
//				});
//
//				AlertDialog al= dialog.show();
//				 TextView titleView = (TextView)al.findViewById(splash.getResources().getIdentifier("alertTitle", "id", "android"));
//				 if (titleView != null) {
//					    titleView.setGravity(Gravity.CENTER);
//					}
//				 TextView messageView = (TextView)al.findViewById(android.R.id.message);
//				 messageView.setGravity(Gravity.CENTER);
//			}
//		});
//
//	}
//	public void createDialogSign1(final Activity splash, final String string) {
//		// TODO Auto-generated method stub
//
//
//
//		splash.runOnUiThread(new Runnable() {
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//
//
//
//
//				 Builder dialog = new Builder(splash);
//
//				 dialog.setTitle("SsamTalk");
//				 dialog.setMessage(string);
//
//				// dialog.
//				 dialog.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
//
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						// TODO Auto-generated method stub
//						dialog.dismiss();
//					//	splash.finish();
//
//						splash.startActivity(new Intent(splash, LoginActivitypatient.class));
//						splash.finish();
//
//					}
//				});
//
//				AlertDialog al= dialog.show();
//				 TextView titleView = (TextView)al.findViewById(splash.getResources().getIdentifier("alertTitle", "id", "android"));
//				 if (titleView != null) {
//					    titleView.setGravity(Gravity.CENTER);
//					}
//				 TextView messageView = (TextView)al.findViewById(android.R.id.message);
//				 messageView.setGravity(Gravity.CENTER);
//			}
//		});
//
//	}
//
//	public void createDialogSignUpto(final Activity splash, final String string) {
//		// TODO Auto-generated method stub
//
//
//
//		splash.runOnUiThread(new Runnable() {
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//
//
//
//
//				 Builder dialog = new Builder(splash);
//
//				 dialog.setTitle("SsamTalk");
//				 dialog.setMessage(string);
//
//				// dialog.
//				 dialog.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
//
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						// TODO Auto-generated method stub
//						dialog.dismiss();
//					//	splash.finish();
//
//						splash.startActivity(new Intent(splash, UserListpatient.class));
//						splash.finish();
//
//					}
//				});
//
//				AlertDialog al= dialog.show();
//				 TextView titleView = (TextView)al.findViewById(splash.getResources().getIdentifier("alertTitle", "id", "android"));
//				 if (titleView != null) {
//					    titleView.setGravity(Gravity.CENTER);
//					}
//				 TextView messageView = (TextView)al.findViewById(android.R.id.message);
//				 messageView.setGravity(Gravity.CENTER);
//			}
//		});
//
//	}
//
//
//
//
//
//
//
//
//
//
//
//
//	public void createDialogCallPasscode(final Activity splash, final String string) {
//		// TODO Auto-generated method stub
//
//
//
//		splash.runOnUiThread(new Runnable() {
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//
//
//
//
//				 Builder dialog = new Builder(splash);
//
//				 dialog.setTitle("eCPD app");
//				 dialog.setMessage(string);
//
//				// dialog.
//				 dialog.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
//
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						// TODO Auto-generated method stub
//						dialog.dismiss();
//
//
//					}
//				});
//
//				AlertDialog al= dialog.show();
//				 TextView titleView = (TextView)al.findViewById(splash.getResources().getIdentifier("alertTitle", "id", "android"));
//				 if (titleView != null) {
//					    titleView.setGravity(Gravity.CENTER);
//					}
//			}
//		});
//
//	}


}
