package com.smartcrop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by insonix on 22/1/16.
 */
public class Help extends Activity {
    ImageView back,setting,plus;
    TextView smartcrop;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help);
        back=(ImageView)findViewById(R.id.menu);
        smartcrop=(TextView)findViewById(R.id.smartcrop);
        setting=(ImageView)findViewById(R.id.setting);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Help.this, SettingScrren.class);
                startActivity(intent);
                finish();
            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Help.this,SettingScrren.class);
                startActivity(intent);
//                finish();
            }

        });
        smartcrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Help.this, HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
