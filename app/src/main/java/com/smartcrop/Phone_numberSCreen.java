package com.smartcrop;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONObject;

import java.io.IOException;
import java.sql.Connection;
import java.util.Locale;

/**
 * Created by insonix on 10/11/15.
 */
public class Phone_numberSCreen extends Activity {
    Button conti;
    EditText phn_num;
    ProgressDialog progressDialog;
    TextView county_code;
    TelephonyManager telephonyManager;
    String county_cod, url;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private String regid = null;
    ToggleButton toggleButton,toggleButton2,toggleButton3;
    private Context context= null;
    protected String SENDER_ID="252890932489";
    private GoogleCloudMessaging gcm=null;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    TextView textView2,textView1,textView3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_number);
        conti = (Button) findViewById(R.id.conti);
        phn_num = (EditText) findViewById(R.id.phn_num);
        county_code = (TextView) findViewById(R.id.county_code);
       progressDialog=new ProgressDialog(Phone_numberSCreen.this);
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor=preferences.edit();
        toggleButton=(ToggleButton)findViewById(R.id.toggleButton11);

        toggleButton2=(ToggleButton)findViewById(R.id.toggleButton21);
        toggleButton3=(ToggleButton)findViewById(R.id.toggleButton23);
        textView1=(TextView)findViewById(R.id.textView1);

        textView2=(TextView)findViewById(R.id.textView2);
        textView3=(TextView)findViewById(R.id.textView3);
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        county_cod = telephonyManager.getNetworkCountryIso();
        county_code.setText(county_cod);
        conti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Network.isNetworkAvailable(Phone_numberSCreen.this)) {
                    if(phn_num.getText().toString().length()>=10) {
                        RegisterNumber();
                    }else{
                        Toast.makeText(Phone_numberSCreen.this,"Please enter 10 digit number.",Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(Phone_numberSCreen.this,"No Network Available",Toast.LENGTH_SHORT).show();
                }
            }
        });
        if(checkPlayServices()){
            registerInBackground();
        }else{

        }
        if(preferences.getString("on","").equals("1")){
            String languageToLoad  = "pa";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            textView1.setText("Choose your language English");

            textView2.setText("ਭਾਸ਼ਾ ਚੁਣੋ ਪੰਜਾਬੀ");
            textView3.setText("भाषा चुनो हिंदी");

            toggleButton2.setChecked(true);

            toggleButton.setChecked(false);
            toggleButton3.setChecked(false);
        }else if (preferences.getString("on","").equals("2")){
            String languageToLoad = "hi";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            toggleButton2.setChecked(false);
            toggleButton3.setChecked(true);
            toggleButton.setChecked(false);
            textView1.setText("Choose your language English");

            textView2.setText("ਭਾਸ਼ਾ ਚੁਣੋ ਪੰਜਾਬੀ");
            textView3.setText("भाषा चुनो हिंदी");
        }else{
            String languageToLoad = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            toggleButton2.setChecked(false);
            toggleButton3.setChecked(false);
            toggleButton.setChecked(true);
            textView1.setText("Choose your language English");

            textView2.setText("ਭਾਸ਼ਾ ਚੁਣੋ ਪੰਜਾਬੀ");
            textView3.setText("भाषा चुनो हिंदी");
        }
        toggleButton2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked==true){
                    editor.putString("on","1");
                    editor.commit();
                    String languageToLoad  = "pa";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    toggleButton.setChecked(false);
                    toggleButton3.setChecked(false);
                    textView1.setText("Choose your language English");

                    textView2.setText("ਭਾਸ਼ਾ ਚੁਣੋ ਪੰਜਾਬੀ");
                    textView3.setText("भाषा चुनो हिंदी");
                }else if(toggleButton.isChecked()){
                    editor.putString("on", "0");
                    editor.commit();

                    toggleButton.setChecked(true);
                    toggleButton3.setChecked(false);
                    toggleButton2.setChecked(false);
                    textView1.setText("Choose your language English");

                    textView2.setText("ਭਾਸ਼ਾ ਚੁਣੋ ਪੰਜਾਬੀ");
                    textView3.setText("भाषा चुनो हिंदी");

                }else{
                    editor.putString("on", "2");
                    editor.commit();
                    toggleButton2.setChecked(false);
                    toggleButton.setChecked(false);
                    toggleButton3.setChecked(true);
                    textView1.setText("Choose your language English");

                    textView2.setText("ਭਾਸ਼ਾ ਚੁਣੋ ਪੰਜਾਬੀ");
                    textView3.setText("भाषा चुनो हिंदी");
                }
            }
        });
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked==true){
                    editor.putString("on","0");
                    editor.commit();
                    String languageToLoad  = "en";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    toggleButton2.setChecked(false);
                    toggleButton3.setChecked(false);

                    textView1.setText("Choose your language English");
                    textView2.setText("ਭਾਸ਼ਾ ਚੁਣੋ ਪੰਜਾਬੀ");
                    textView3.setText("भाषा चुनो हिंदी");
                }else if(toggleButton2.isChecked()){
                    editor.putString("on","1");
                    editor.commit();
                    toggleButton.setChecked(false);
                    toggleButton3.setChecked(false);
                    toggleButton2.setChecked(true);
                    textView1.setText("Choose your language English");
                    textView2.setText("ਭਾਸ਼ਾ ਚੁਣੋ ਪੰਜਾਬੀ");
                    textView3.setText("भाषा चुनो हिंदी");

                }else{
                    editor.putString("on", "2");
                    editor.commit();
                    toggleButton2.setChecked(false);
                    toggleButton.setChecked(false);
                    toggleButton3.setChecked(true);
                    textView1.setText("Choose your language English");

                    textView2.setText("ਭਾਸ਼ਾ ਚੁਣੋ ਪੰਜਾਬੀ");
                    textView3.setText("भाषा चुनो हिंदी");
                }
            }
        });
        toggleButton3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked==true){
                    editor.putString("on","2");
                    editor.commit();
                    String languageToLoad  = "hi";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    toggleButton2.setChecked(false);
                    toggleButton.setChecked(false);

                    textView1.setText("Choose your language English");
                    textView2.setText("ਭਾਸ਼ਾ ਚੁਣੋ ਪੰਜਾਬੀ");
                    textView3.setText("भाषा चुनो हिंदी");
                }else if(toggleButton2.isChecked()){
                    editor.putString("on","1");
                    editor.commit();
                    toggleButton.setChecked(false);
                    toggleButton3.setChecked(false);
                    toggleButton2.setChecked(true);
                    textView1.setText("Choose your language English");
                    textView2.setText("ਭਾਸ਼ਾ ਚੁਣੋ ਪੰਜਾਬੀ");
                    textView3.setText("भाषा चुनो हिंदी");

                }else{
                    editor.putString("on", "0");
                    editor.commit();
                    toggleButton2.setChecked(false);
                    toggleButton.setChecked(true);
                    toggleButton3.setChecked(false);
                    textView1.setText("Choose your language English");

                    textView2.setText("ਭਾਸ਼ਾ ਚੁਣੋ ਪੰਜਾਬੀ");
                    textView3.setText("भाषा चुनो हिंदी");
                }
            }
        });


    }

    public void RegisterNumber() {
        String androidId = Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url="http://52.35.22.61/smart_crop/save_user_contact.php?phone_number="+phn_num.getText().toString()+"&device_id="+androidId;
        Log.d("urlis", "device url " + url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:","resss"+jsonObject.toString());
        try{
        String message=jsonObject.getString("Message");
            if(message.equalsIgnoreCase("Phone number updated successfully")) {
                String Status = jsonObject.getString("Status");
                String id = jsonObject.getString("ID");
                String Phone_Number = jsonObject.getString("Phone_Number");
                editor.putString("uid", id);
                editor.putString("Status", Status);
                editor.putString("Phone_Number", Phone_Number);
                editor.commit();
                Intent intent = new Intent(Phone_numberSCreen.this, HomeCategory.class);
                startActivity(intent);
                finish();
            }else{
                Toast.makeText(Phone_numberSCreen.this,"Please try again.",Toast.LENGTH_SHORT).show();
            }

        }catch(Exception e){
            Log.d("eeee:","eee"+e);
        }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    @Override protected void onResume()
    {
        super.onResume();
        checkPlayServices();
    }
    private boolean checkPlayServices() {
        int resultcode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (resultcode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultcode)) {
                GooglePlayServicesUtil.getErrorDialog(resultcode, Phone_numberSCreen.this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {

            }
            return false;
        }
        return true;
    }
public void  registerInBackground(){
    new AsyncTask<Void,Void,String>(){

        protected String doInBackground(Void... params) {
            String msg = "";
            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging
                            .getInstance(getApplicationContext());
                }
                regid = gcm.register(SENDER_ID);
                msg = "Registration ID :" + regid;
                Log.i("Registration id", regid);

                editor.putString("reg_id", regid);
                editor.commit();


            } catch (IOException ex) {
                msg = "Error :" + ex.getMessage();
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String msg) {
            if (!TextUtils.isEmpty(regid)) {
                // storeRegIdinSharedPref(getActivity(), regId, emailID);
						/*
						 * Toast.makeText( con,
						 * "Registered with GCM Server successfully.\n\n" + msg,
						 * Toast.LENGTH_SHORT).show();
						 */
            } else {
						/*
						 * Toast.makeText( con,
						 * "Reg ID Creation Failed.\n\nEither you haven't enabled Internet or GCM server is busy right now. Make sure you enabled Internet and try registering again after some time."
						 * + msg, Toast.LENGTH_LONG).show();
						 */
            }
        }
    }.execute(null, null, null);
}

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//
//        Intent startMain = new Intent(Intent.ACTION_MAIN);
//        startMain.addCategory(Intent.CATEGORY_HOME);
//        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(startMain);
        finish();
    }
}


