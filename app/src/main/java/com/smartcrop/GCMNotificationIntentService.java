package com.smartcrop;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.util.Iterator;
import java.util.Set;

public class GCMNotificationIntentService extends IntentService {
	// Sets an ID for the notification, so it can be updated
	//static final String MSG_KEY = "m";
	public static final int notifyID = 9001;
	NotificationCompat.Builder builder;
	SharedPreferences preferences ;
	SharedPreferences.Editor editor;
	String getclass,newString;


	public GCMNotificationIntentService() {
		super("GcmIntentService");

	}


	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		//extras.get("message");
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

		String messageType = gcm.getMessageType(intent);


		if (!extras.isEmpty()) {
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {
				sendNotification("Send error: " + extras.toString(),""+extras.toString(),""+extras.toString(),"" +extras.toString(),
						"" +extras.toString(), "" +extras.toString(),""+extras.toString(),"" +extras.toString(),"" +extras.toString(),""+extras.toString(),"" +extras.toString(),
						"" +extras.toString(),"" +extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {
				sendNotification("Deleted messages on server: "
						+ extras.toString(),""+extras.toString(),""+extras.toString(),"" +extras.toString(),"" +extras.toString(), ""
						+ extras.toString(),""+extras.toString(),""+extras.toString(),"" +extras.toString(),"" +extras.toString(),"" +extras.toString(),"" +extras.toString(),"" +extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType)) {
//				if(preferences.getBoolean("checked",true)) {
				sendNotification(" "
						+ extras.get("message"), "" + extras.get("uid"), "" + extras.get("chat_id"), "" +extras.get("name"), "" +extras.get("sub_category_id"),"" + extras.get("cat"), "" + extras.get("quantity"), "" + extras.get("max"), "" + extras.get("min"), "" + extras.get("state"), "" + extras.get("phone"), "" + extras.get("description") , "" + extras.get("category_image"));
				Log.d("UName", "UName" + extras.get("name"));
//				}else{
//
//				}
				Set<String> sss= extras.keySet();
				Iterator it=sss.iterator();
				while (it.hasNext()) {
					Log.d("iteration", "it"+it.next());


				}
			}
		}
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void sendNotification(String msg,String uid,String chatid,String name,String sub_category_id,String cat,String quantity,String max, String min,String state,String phone,String description,String category_image) {
		preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		editor = preferences.edit();
		PendingIntent resultPendingIntent;
		Intent resultIntent;
		String message=msg.trim();
//		if(getclass.equals("TAB")){
//			resultIntent = new Intent(this, LeasingtckerTabActivity.class);
//			resultIntent.putExtra("msg",msg);
//
//			resultIntent.putExtra("tab","TAB");
//		}else{
//		if(check.equals("check")){
//			resultIntent = new Intent(this, HomeCategory.class);
//			resultIntent.putExtra("msg",msg);
//
//
// }else{
		if(message.equals("A new post is added to sell in your wishlist")){
			Log.d("userID:","userID"+ uid);
			Log.d("sub_category_id:","sub_category_id"+ sub_category_id);
			Log.d("userName:","userName"+ name);
			Log.d("msg:","msg"+ msg);
			resultIntent = new Intent(this, SingleCategory.class);
			resultIntent.putExtra("msg",msg);
			resultIntent.putExtra("uid",uid);
			resultIntent.putExtra("chatid",chatid);
			resultIntent.putExtra("name",name);
			resultIntent.putExtra("sub_category_id",sub_category_id);
			resultIntent.putExtra("cat",cat);
			resultIntent.putExtra("quantity",quantity);
			resultIntent.putExtra("max",max);
			resultIntent.putExtra("min",min);
			resultIntent.putExtra("state",state);
			resultIntent.putExtra("phone",phone);
			resultIntent.putExtra("description",description);
			resultIntent.putExtra("category_image",category_image);
			resultIntent.putExtra("back", "sell");
			resultPendingIntent = PendingIntent.getActivity(this, 0,
					resultIntent, PendingIntent.FLAG_ONE_SHOT);
//
			NotificationCompat.Builder mNotifyBuilder;
			NotificationManager mNotificationManager;

			mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

			mNotifyBuilder = new NotificationCompat.Builder(this)
					.setSmallIcon(R.drawable.logo).setContentText(msg).setContentTitle("Smartcrop");
			// Set pending intent
			mNotifyBuilder.setContentIntent(resultPendingIntent);

			// Set Vibrate, Sound and Light
			int defaults = 0;
			defaults = defaults | Notification.DEFAULT_LIGHTS;
			defaults = defaults | Notification.DEFAULT_VIBRATE;
			defaults = defaults | Notification.DEFAULT_SOUND;

			mNotifyBuilder.setDefaults(defaults);
			// Set the content for Notification
			// mNotifyBuilder.setContentText(msg);
			// Set autocancel
			mNotifyBuilder.setAutoCancel(true);
			// Post a notification
			mNotificationManager.notify(notifyID, mNotifyBuilder.build());
//
		}
		else {
			Log.d("msgelse:","msgelse"+msg);
			resultIntent = new Intent(this, ChatActivity.class);
			resultIntent.putExtra("msg", msg);
			resultIntent.putExtra("chat_id", chatid);
			resultIntent.putExtra("id", uid);
			resultIntent.putExtra("name", name);
//			resultIntent.putExtra("back", "sell");
			resultPendingIntent = PendingIntent.getActivity(this, 0,
					resultIntent, PendingIntent.FLAG_ONE_SHOT);
//
			NotificationCompat.Builder mNotifyBuilder;
			NotificationManager mNotificationManager;

			mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

			mNotifyBuilder = new NotificationCompat.Builder(this)
					.setSmallIcon(R.drawable.logo).setContentText(msg).setContentTitle("Smartcrop");
			// Set pending intent
			mNotifyBuilder.setContentIntent(resultPendingIntent);

			// Set Vibrate, Sound and Light
			int defaults = 0;
			defaults = defaults | Notification.DEFAULT_LIGHTS;
			defaults = defaults | Notification.DEFAULT_VIBRATE;
			defaults = defaults | Notification.DEFAULT_SOUND;

			mNotifyBuilder.setDefaults(defaults);
			// Set the content for Notification
			// mNotifyBuilder.setContentText(msg);
			// Set autocancel
			mNotifyBuilder.setAutoCancel(true);
			// Post a notification
			mNotificationManager.notify(notifyID, mNotifyBuilder.build());
			// }
		}
//		resultIntent.putExtra("photo", image);

//
		resultPendingIntent = PendingIntent.getActivity(this, 0,
				resultIntent, PendingIntent.FLAG_ONE_SHOT);
//
		NotificationCompat.Builder mNotifyBuilder;
		NotificationManager mNotificationManager;

		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		mNotifyBuilder = new NotificationCompat.Builder(this)
				.setSmallIcon(R.drawable.logo).setContentText(msg).setContentTitle("Smartcrop");
		// Set pending intent
		mNotifyBuilder.setContentIntent(resultPendingIntent);

		// Set Vibrate, Sound and Light
		int defaults = 0;
		defaults = defaults | Notification.DEFAULT_LIGHTS;
		defaults = defaults | Notification.DEFAULT_VIBRATE;
		defaults = defaults | Notification.DEFAULT_SOUND;

		mNotifyBuilder.setDefaults(defaults);
		// Set the content for Notification
		// mNotifyBuilder.setContentText(msg);
		// Set autocancel
		mNotifyBuilder.setAutoCancel(true);
		// Post a notification
		mNotificationManager.notify(notifyID, mNotifyBuilder.build());
	}


	private Object getFragmentManager() {
		// TODO Auto-generated method stub
		return null;
	}
}