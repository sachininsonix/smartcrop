package com.smartcrop;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by insonix on 17/11/15.
 */
public class SubCategory extends Activity {
    ListView sub_category;
    TextView category_name,smartcrop;
    EditText search_category;
    String catname,url,catid,city_id;
    ProgressDialog progressDialog;
    ArrayList<HashMap<String,String >>addme;
    ArrayList<HashMap<String,String >>searchlist;
    SubCategoryAdapter subCategoryAdapter;
    ImageView back,setting;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sub_category);
        search_category=(EditText)findViewById(R.id.search_category);
        sub_category=(ListView)findViewById(R.id.sub_category);
        category_name=(TextView)findViewById(R.id.all);
        smartcrop=(TextView)findViewById(R.id.smartcrop);
        progressDialog=new ProgressDialog(SubCategory.this);
        addme=new ArrayList<>();
//        searchlist=new ArrayList<>();


        try {
            catname = getIntent().getExtras().getString("catname");
            catid = getIntent().getExtras().getString("catid");
            city_id = getIntent().getExtras().getString("city_id");
            Log.d("city_id","city_id"+city_id);
        }catch (Exception e){

        }
        category_name.setText(catname);
        back=(ImageView)findViewById(R.id.menu);
        setting=(ImageView)findViewById(R.id.setting);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubCategory.this, AllCategory.class);
                intent.putExtra("city_id", city_id);
                startActivity(intent);
                finish();
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubCategory.this, SettingScrren.class);
                startActivity(intent);
//                finish();
            }
        });
        smartcrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubCategory.this, HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });
        SubCategoryList();
        search_category.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String searchString=search_category.getText().toString();
                int textLength=searchString.length();
                searchlist.clear();
                for(int i=0;i<addme.size();i++){
                    String catename=addme.get(i).get("sub_cat_name").toString();
                    if(textLength<=catename.length()){
                        if(searchString.equalsIgnoreCase(catename.substring(0,textLength)))
                            searchlist.add(addme.get(i));

                    }
                }
//                sub_category.setAdapter(subCategoryAdapter);
                subCategoryAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void SubCategoryList(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url="http://52.35.22.61/smart_crop/get_selected_categories.php?id="+catid;
        Log.d("urlvvv","url"+url);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("Status");
                if(status.equalsIgnoreCase("OK")){
                    JSONArray jsonArray=jsonObject.getJSONArray("subcategory");
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject catlist=jsonArray.getJSONObject(i);
                        String id=catlist.getString("ID");
                        String CatName=catlist.getString("Category_name");
                       String sub_cat_name=catlist.getString("sub_cat_name");
                        Log.d("CatName", "CatName" + CatName);
                        HashMap<String,String> hashMap=new HashMap<>();
                        hashMap.put("catname",CatName);
                        hashMap.put("sub_cat_name",sub_cat_name);

                        hashMap.put("catid",id);
                        addme.add(hashMap);

                    }
                    searchlist=new ArrayList<>(addme);
                    subCategoryAdapter=new SubCategoryAdapter(SubCategory.this, searchlist);
                    sub_category.setAdapter(subCategoryAdapter);

                }else {
                    new ShowMsg().createDialog(SubCategory.this,"No SubCategory");
                }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class SubCategoryAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> searchlist;
        public SubCategoryAdapter(Activity activity,ArrayList<HashMap<String,String>>searchlist){
            this.activity=activity;
            this.searchlist=searchlist;
        }
        @Override
        public int getCount() {
            return searchlist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.all_category_item,null);
//            ImageView cat_image=(ImageView)convertView.findViewById(R.id.cat_image);
            TextView cat_name=(TextView)convertView.findViewById(R.id.cat_name);
            RelativeLayout rel_help=(RelativeLayout)convertView.findViewById(R.id.rel_help);
            cat_name.setText(searchlist.get(position).get("sub_cat_name").toString());
            rel_help.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(activity,SellingList.class);
                    intent.putExtra("subcat_id",searchlist.get(position).get("catid"));
                    intent.putExtra("catname",catname);
                    intent.putExtra("sub_cat_name",searchlist.get(position).get("sub_cat_name"));

                    activity.startActivity(intent);
                    finish();
                }
            });


            return convertView;
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent(SubCategory.this, HomeCategory.class);
//        intent.putExtra("city_id", city_id);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
