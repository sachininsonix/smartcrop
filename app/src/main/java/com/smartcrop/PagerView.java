package com.smartcrop;

/**
 * Created by insonix on 22/2/16.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;


public class PagerView extends Activity {

    private ImageView imageViewDot;
    private ViewPager viewPager;
    private ImagePagerAdapter adapter;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;


    Button getstarted;

    private int[] mImages = new int[] { R.drawable.pager, R.drawable.pager1,
            R.drawable.pager2, R.drawable.pager3, R.drawable.pager4 };

    private int[] mImagesDot = new int[] { R.drawable.scroll_dot1,
            R.drawable.scroll_dot2, R.drawable.scroll_dot3,

            R.drawable.scroll_dot4, R.drawable.scroll_dot5 };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.viewpager);
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor=preferences.edit();
        getstarted=(Button)findViewById(R.id.getstartedbutton);

        imageViewDot = (ImageView) this.findViewById(R.id.imageView_dot);

        viewPager = (ViewPager) this.findViewById(R.id.view_pager);
        adapter = new ImagePagerAdapter();
        viewPager.setAdapter(adapter);

        viewPager.setOnPageChangeListener(new OnPageChangeListener() {

            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub

            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub
                imageViewDot.setImageResource(mImagesDot[arg0]);
            }

            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });

        getstarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (preferences.getString("uid", "").equals("")) {
                    Intent intent = new Intent(PagerView.this, Phone_numberSCreen.class);
                    startActivity(intent);
                    finish();
//                } else {
//                    Intent intent = new Intent(PagerView.this, HomeCategory.class);
//                    startActivity(intent);
//                    finish();
//                }
            }
            });

        }

        private class ImagePagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return mImages.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((ImageView) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Context context = PagerView.this;

            ImageView imageView = new ImageView(context);

            int padding = context.getResources().getDimensionPixelSize(
                    R.dimen.padding_medium);
            imageView.setPadding(padding, padding, padding, padding);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            imageView.setImageResource(mImages[position]);

            ((ViewPager) container).addView(imageView, 0);

            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((ImageView) object);

        }

    }

}