package com.smartcrop;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FirebaseListAdapter<T> extends BaseAdapter {
	private Query mRef;
	private Class<Chat> mModelClass;
	 private int mLayout,thirdLayoutChat,fourthLayoutChat,secondLayout;
	 private LayoutInflater mInflater;
	    private List<Chat> mModels;
	    String userName,chat_id;
	    SharedPreferences pref;
	    ArrayList<String> keysFireBase;
	    SharedPreferences.Editor editor;
	    private Map<String, Chat> mModelKeys;
	    private ChildEventListener mListener;
	    Activity activity;
	    Chat chat;
	ImageLoader imageLoader;
	DisplayImageOptions options;

	public FirebaseListAdapter(Query mRef, Class<Chat> mModelClass, int mLayout,
			int secondLayout, Activity activity, String chat_id) {
		// TODO Auto-generated constructor stub
		this.mRef=mRef;
		this.mModelClass=mModelClass;
		this.mLayout=mLayout;
		this.secondLayout=secondLayout;
		this.thirdLayoutChat=thirdLayoutChat;
		this.fourthLayoutChat=fourthLayoutChat;
		this.activity=activity;
		this.chat_id = chat_id;
		imageLoader=ImageLoader.getInstance();
		imageLoader.init(ImageLoaderConfiguration.createDefault(activity));
		options=new  DisplayImageOptions.Builder().showStubImage(R.drawable.images).showImageForEmptyUri(R.drawable.profile_image).cacheOnDisc().cacheInMemory().build();

		keysFireBase=new ArrayList<>();
        mInflater = activity.getLayoutInflater();
        mModels = new ArrayList<Chat>();
        pref = PreferenceManager.getDefaultSharedPreferences(activity);
        userName=pref.getString("uid", " ");
        Log.d("userName", "userName"+userName);
        editor=pref.edit();    
        mModelKeys = new HashMap<String, Chat>();
        mListener = this.mRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
            	Chat model = dataSnapshot.getValue(FirebaseListAdapter.this.mModelClass);
                mModelKeys.put(dataSnapshot.getKey(), model);
                Log.i("LOC", "Keys:"+dataSnapshot.getKey());
                keysFireBase.add(dataSnapshot.getKey());
                // Insert into the correct location, based on previousChildName
                if (previousChildName == null) {
                    mModels.add(0, model);
                } else {
                	Chat previousModel = mModelKeys.get(previousChildName);
                    int previousIndex = mModels.indexOf(previousModel);
                    int nextIndex = previousIndex + 1;
                    if (nextIndex == mModels.size()) {
                        mModels.add(model);
                    } else {
                        mModels.add(nextIndex, model);
                    }
                }
                notifyDataSetChanged();
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                // One of the mModels changed. Replace it in our list and name mapping
                String modelName = dataSnapshot.getKey();
                Chat oldModel = mModelKeys.get(modelName);
                Chat newModel = dataSnapshot.getValue(FirebaseListAdapter.this.mModelClass);
                int index = mModels.indexOf(oldModel);
                mModels.set(index, newModel);
                mModelKeys.put(modelName, newModel);
                notifyDataSetChanged();
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                // A model was removed from the list. Remove it from our list and the name mapping
                String modelName = dataSnapshot.getKey();
                Chat oldModel = mModelKeys.get(modelName);
                mModels.remove(oldModel);
                mModelKeys.remove(modelName);
                notifyDataSetChanged();
            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                // A model changed position in the list. Update our list accordingly
                String modelName = dataSnapshot.getKey();
                Chat oldModel = mModelKeys.get(modelName);
                Chat newModel = dataSnapshot.getValue(FirebaseListAdapter.this.mModelClass);
                int index = mModels.indexOf(oldModel);
                mModels.remove(index);
                if (previousChildName == null) {
                    mModels.add(0, newModel);
                } else {
                	Chat previousModel = mModelKeys.get(previousChildName);
                    int previousIndex = mModels.indexOf(previousModel);
                    int nextIndex = previousIndex + 1;
                    if (nextIndex == mModels.size()) {	
                        mModels.add(newModel);     
                    } else {                	
                        mModels.add(nextIndex, newModel);    
                    }
                }
                notifyDataSetChanged();
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e("FirebaseListAdapter", "Listen was cancelled, no more updates will occur");
            }
        });
    
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mModels.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mModels.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		chat=mModels.get(position);
		Log.d("chatid", "chat"+chat.getChatID());
		Log.d("uername", "userName"+userName);
		
		if(chat.getChatID().equals(userName)) {

            if (chat.getType().equalsIgnoreCase("message")) {
                convertView = mInflater.inflate(secondLayout, null);
                TextView singleMessage = (TextView) convertView.findViewById(R.id.singleMessage1);
//
                singleMessage.setText(chat.getMessage());
                TextView time = (TextView) convertView.findViewById(R.id.textView_timeChatOthers);
                time.setText(chat.getTime());
//

//
//
            }
        }else {
                if (chat.getType().equalsIgnoreCase("message")) {
                    convertView = mInflater.inflate(mLayout, null);
                    TextView singleMessage = (TextView) convertView.findViewById(R.id.singleMessage);
//
                    singleMessage.setText(chat.getMessage());
                    TextView time = (TextView) convertView.findViewById(R.id.textView_timeChat);
                    time.setText(chat.getTime());
//
                }
//
//
            }

		return convertView;
	}

	public void cleanup() {
		// TODO Auto-generated method stub
		 mRef.removeEventListener(mListener);
	        mModels.clear();
	        mModelKeys.clear();
	}
	public static Bitmap decodeSampledBitmapFromByte(byte[] decodedByte,
            int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length, options);
      //  BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return  BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length, options);
    }
	public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;

    if (height > reqHeight || width > reqWidth) {

        final int halfHeight = height / 2;
        final int halfWidth = width / 2;

        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        // height and width larger than the requested height and width.
        while ((halfHeight / inSampleSize) > reqHeight
                && (halfWidth / inSampleSize) > reqWidth) {
            inSampleSize *= 2;
        }
    }

    return inSampleSize;
}

}
