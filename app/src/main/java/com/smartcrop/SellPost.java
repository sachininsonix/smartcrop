package com.smartcrop;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by insonix on 17/11/15.
 */
public class SellPost extends Activity {
    EditText sell_price,sell_name,address,desc,choosequan,item_name,subcat,phone_nu,dist,maxprice,minprice;
    RelativeLayout sell_city;
    TextView city,smartcrop;
    ScrollView scrollView;
    String subcat_id,city_id,cat_name,sub_name;
    String url,url1,file,pri_text,quan_text;
    Button submit;
    private Calendar calendar;
    private TextView dateView;
    private int year, month, day;
    double total;

    static int w=0, h=0;
    Dialog imageDialog;
    String[]quan={"/kg","/gm","/ton"};
    String[]price={"kg","gm","ton"};
    protected  static final int IMAGE_CAMERA_11=11;
    protected static final int IMAGE_GALLERY_22 = 22;
    Bitmap bitmap;

    SharedPreferences.Editor editor;
    ArrayList<HashMap<String,String >>addme;
    ProgressDialog progressDialog;
    SharedPreferences preferences;
    ListView list_city;
    int maxi,mini;
    ImageView back,setting,sell_image;
    Spinner spin_qua,spin_pri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Display display = getWindowManager().getDefaultDisplay();
        w = display.getWidth();
        h= display.getHeight();
        setContentView(R.layout.sell_post);
        progressDialog=new ProgressDialog(SellPost.this);
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor=preferences.edit();
        desc=(EditText)findViewById(R.id.desc);
        dist=(EditText)findViewById(R.id.dist);
        scrollView=(ScrollView)findViewById(R.id.scroll);
        item_name=(EditText)findViewById(R.id.item_name);
        addme=new ArrayList<>();
        dateView=(TextView)findViewById(R.id.dateView);
        spin_qua=(Spinner)findViewById(R.id.spin_qua);
        spin_pri=(Spinner)findViewById(R.id.spin_pri);
        sell_city=(RelativeLayout)findViewById(R.id.sell_city);
        list_city=(ListView)findViewById(R.id.list_city);
        sell_name=(EditText)findViewById(R.id.sell_name);
        phone_nu=(EditText)findViewById(R.id.phone_nu);
        address=(EditText)findViewById(R.id.address);
        submit=(Button)findViewById(R.id.submit);
//        sell_price=(EditText)findViewById(R.id.sell_price);
        subcat=(EditText)findViewById(R.id.subcat);
        sell_image=(ImageView)findViewById(R.id.sell_pic);
        back=(ImageView)findViewById(R.id.menu);
        setting=(ImageView)findViewById(R.id.setting);
//        state=(TextView)findViewById(R.id.state);
        choosequan=(EditText)findViewById(R.id.choose_quan);
        city=(TextView)findViewById(R.id.city);
        smartcrop=(TextView)findViewById(R.id.smartcrop);
        phone_nu.setText(preferences.getString("Phone_Number",""));
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        maxprice=(EditText)findViewById(R.id.maxprice);
        minprice=(EditText)findViewById(R.id.minprice);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month + 1, day);
        try {
            subcat_id = getIntent().getExtras().getString("subcat_id");
            cat_name = getIntent().getExtras().getString("catname");
            sub_name = getIntent().getExtras().getString("sub_cat_name");
            Log.d("subcat_id:", "subcat_id" + subcat_id);
        }catch(Exception e){

        }
        if(preferences.getString("username","").equals("")){
            sell_name.setText("");
        }else{
            sell_name.setText(preferences.getString("username",""));
        }
        if(preferences.getString("adress","").equals("")){
            address.setText("");
        }else{
            address.setText(preferences.getString("adress",""));
        }
        if(preferences.getString("dist","").equals("")){
            dist.setText("");
        }else{
            dist.setText(preferences.getString("dist",""));
        }
        imageDialog = new Dialog(this);
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = imageDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        imageDialog.setContentView(R.layout.options);
        LinearLayout approx_lay = (LinearLayout) imageDialog.findViewById(R.id.approx_lay);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(w - 30, (h / 3) - 20);
        approx_lay.setLayoutParams(params);
        ArrayAdapter<String>adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,quan);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<String>adapter1=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,price);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_qua.setAdapter(adapter1);
        spin_pri.setAdapter(adapter);
        spin_qua.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                quan_text = spin_qua.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spin_pri.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pri_text = spin_pri.getSelectedItem().toString();
                Log.d("pri_text", "pri_text" + pri_text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sell_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageDialog.show();
            }
        });
        item_name.setText(cat_name);
        subcat.setText(sub_name);
        if(preferences.getString("cname","").equals("")){

        }else{
          city.setText(preferences.getString("cname",""));
        }

        TextView Invite_mail = (TextView) imageDialog.findViewById(R.id.options_camera);
        Invite_mail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//					startActivityForResult(intent, IMAGE_CAMERA_11);

                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, IMAGE_CAMERA_11);
                imageDialog.dismiss();
            }
        });
        TextView Invite_sms = (TextView) imageDialog.findViewById(R.id.options_gallery);
        Invite_sms.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, IMAGE_GALLERY_22);
                imageDialog.dismiss();
            }
        });
        TextView Invite_cancel = (TextView) imageDialog.findViewById(R.id.options_cancel);
        Invite_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                imageDialog.dismiss();
            }
        });
//
dateView.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        showDialog(999);
    }
});
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (item_name.getText().toString().length() > 0) {
                    if (choosequan.getText().toString().length() > 0) {
                        if (maxprice.getText().toString().length() > 0) {
                            if (minprice.getText().toString().length() > 0) {
                                maxi=Integer.parseInt(maxprice.getText().toString().trim());
                                mini=Integer.parseInt(minprice.getText().toString().trim());
                                if(maxi>mini){

//
//

                                Addpost();
                                Log.d("url","url"+url);
                            }else {
                                    new ShowMsg().createDialog(SellPost.this, "Minimum price should not greater then maximum price ");
                                }
                            } else {
                                new ShowMsg().createDialog(SellPost.this, "Please enter your min price");
                            }
                        } else {
                            new ShowMsg().createDialog(SellPost.this, "Please enter your max price");
                        }
                    } else {
                        new ShowMsg().createDialog(SellPost.this, "Please enter your quantity");
                    }
                }else {
                 editor.putString("username", sell_name.getText().toString());
                    editor.putString("adress", address.getText().toString());
                    editor.putString("dist", dist.getText().toString());
                    editor.commit();       new ShowMsg().createDialog(SellPost.this, "Please enter your item");
                    }


            }
        });
        sell_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                scrollView.setEnabled(false);
                list_city.setVisibility(View.VISIBLE);
                list_city.setAdapter(new CityAdapter(SellPost.this, addme));
            }
        });
        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                scrollView.setEnabled(false);
                list_city.setVisibility(View.VISIBLE);
                list_city.setAdapter(new CityAdapter(SellPost.this, addme));
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(list_city.getVisibility()==View.VISIBLE ){
                    list_city.setVisibility(View.GONE);
                }
                else{
                    Intent intent=new Intent(SellPost.this,SellingList.class);
                    intent.putExtra("subcat_id",subcat_id);
                    intent.putExtra("catname",cat_name);
                    intent.putExtra("sub_cat_name", sub_name);
                    startActivity(intent);
                    finish();
                }


            }
        });
        smartcrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SellPost.this, HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SellPost.this, SettingScrren.class);
                startActivity(intent);
//                finish();
            }
        });


        CityList();
    }
    private void showDate(int year, int month, int day) {
        dateView.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }
    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        return null;
    }
    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
//            selectedYear = year;
//            selectedMonth = month;
//            selectedDay = day;
            showDate(selectedYear, selectedMonth + 1, selectedDay);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case IMAGE_CAMERA_11:
                if (data != null) {
                    Uri uri = data.getData();
                    try {
                        bitmap = new UserPicture(uri, getContentResolver()).getBitmap();
                        sell_image.setImageBitmap(bitmap);
                    } catch (Exception e) {

                    }
                } else {

                }
                break;
            case IMAGE_GALLERY_22:
                if (data != null) {
                    Uri uri = data.getData();
                    try {
                        bitmap = new UserPicture(uri, getContentResolver()).getBitmap();
                        sell_image.setImageBitmap(bitmap);
                    } catch (Exception e) {

                    }
                } else {

                }
                break;
        }
    }
//
//
    public void Addpost() {
        url= "http://52.35.22.61/smart_crop/users_sell.php?";
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Log.d("url,...... ","url isssss"+url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                Log.d("sss", "sss" + s);
                try {
                    JSONObject jsonObject=new JSONObject(s);

//                    String quantity = jsonObject.getString("quantity");
//                    String price = jsonObject.getString("price");
////                    String quantity = jsonObject.getString("quantity");
                    editor.putString("username", sell_name.getText().toString());
                    editor.putString("adress", address.getText().toString());
                    editor.putString("dist", dist.getText().toString());
                    editor.commit();
//                    final Toast toast = Toast.makeText(getApplicationContext(), "Congratulation!your sell post has been published.Please continue to check list of buyers interested in your item", Toast.LENGTH_LONG);
//                    toast.show();
//
//                    Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            toast.cancel();
//                        }
//                    }, 500);
                    Intent intent = new Intent(SellPost.this, Thankyou.class);
                    intent.putExtra("subcat_id", subcat_id);
                    intent.putExtra("catname", cat_name);
                    intent.putExtra("sub_cat_name", sub_name);
//                    intent.putExtra("total",total);
                    startActivity(intent);
                    finish();

//                    Toast.makeText(SellPost.this,"Congratulation!your sell post has been published.Please continue to check list of buyers interested in your item",Toast.LENGTH_LONG).show();

                }catch (Exception e){
                    Log.d("eeeee:", "eeeee" + e);
                }
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
            }


        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                ByteArrayOutputStream arrayOutputStream=new ByteArrayOutputStream();

                if(bitmap==null){

                }else {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, arrayOutputStream);
                    byte[] data = arrayOutputStream.toByteArray();
                    file = Base64.encodeToString(data, Base64.DEFAULT);
                }
                params.put("user_contact_id", preferences.getString("uid",""));
                params.put("sub_category_id", subcat_id);
                params.put("name", sell_name.getText().toString().trim());
                params.put("item",item_name.getText().toString().trim());
                params.put("quantity",choosequan.getText().toString().trim()+""+quan_text);
                params.put("price","");
                params.put("address",address.getText().toString().trim());
                params.put("phone",phone_nu.getText().toString().trim());
                params.put("city_id",city.getText().toString().trim());
                params.put("description",desc.getText().toString().trim());
                params.put("min",minprice.getText().toString().trim());
                params.put("max",maxprice.getText().toString().trim()+""+pri_text);
                params.put("subcategory_name",subcat.getText().toString().trim());
                params.put("district",dist.getText().toString().trim());
                params.put("month_name",dateView.getText().toString().trim());
                params.put("total",""+total);

                if(file==null){
//
                }else {

                    params.put("profile_name", "" + new Date().getTime() + ".png");
                    params.put("profile_file", file);
                }

                return params;
            }
//            private Map<String, String> checkParams(Map<String, String> map){
//                Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
//                while (it.hasNext()) {
//                    Map.Entry<String, String> pairs = (Map.Entry<String, String>)it.next();
//                    if(pairs.getValue()==null){
//                        map.put(pairs.getKey(), "");
//                    }
//                }
//                return map;
//            }
//
//
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
    public void CityList(){
//        progressDialog.setMessage("Loading...");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
        url1="http://52.35.22.61/smart_crop/get_all_cities.php";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("Status");

                    JSONArray jsonArray=jsonObject.getJSONArray("City");
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject City=jsonArray.getJSONObject(i);
                        String id=City.getString("ID");
                        String City_Name=City.getString("City_Name");

                        Log.d("CatName", "CatName" + City_Name);
                        HashMap<String,String> hashMap=new HashMap<>();
                        hashMap.put("City_Name",City_Name);
                        hashMap.put("City_id",id);

                        addme.add(hashMap);
                    }
                    list_city.setAdapter(new CityAdapter(SellPost.this, addme));



                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
//                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class CityAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> addme;
        public CityAdapter(Activity activity,ArrayList<HashMap<String,String>>addme){
            this.activity=activity;
            this.addme=addme;
        }
        @Override
        public int getCount() {
            return addme.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.city_item,null);

            TextView city_name=(TextView)convertView.findViewById(R.id.city_name);
            city_name.setText(addme.get(position).get("City_Name"));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list_city.setVisibility(View.GONE);
                    city.setText(addme.get(position).get("City_Name"));
//                    city_id=addme.get(position).get("City_id");
                }
            });

            return convertView;
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if(list_city.getVisibility()==View.VISIBLE ){
            list_city.setVisibility(View.GONE);
        }
        else{
            Intent intent=new Intent(SellPost.this,SellingList.class);
            intent.putExtra("subcat_id",subcat_id);
            intent.putExtra("catname",cat_name);
            intent.putExtra("sub_cat_name", sub_name);
            startActivity(intent);
            finish();
        }
    }
}