package com.smartcrop;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by insonix on 19/1/16.
 */
public class EditBuyPost extends Activity {
    EditText sell_price,sell_name,address,desc,choosequan,item_name,subcat,phone_nu,dist,maxprice,minprice;
    RelativeLayout sell_city;
    TextView city,deta;
    int position;
    ScrollView scrollView;
    String subcat_id,city_id,cat_name,sub_name,priic,priun,aplhabetonly;
    String url,url1,file,pri_text,quan_text,id,numberOnly;
    Button submit;
    int maxi,mini;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    static int w=0, h=0;
    ArrayList<HashMap<String,String>>hashme;
    SharedPreferences.Editor editor;
    double total;
    Dialog imageDialog;
    String[]quan={"/kg","/gm","/ton"};
    String[]price={"kg","gm","ton"};
    protected  static final int IMAGE_CAMERA_11=11;
    protected static final int IMAGE_GALLERY_22 = 22;
    Bitmap bitmap;

    ArrayList<HashMap<String,String >> addme;
    ProgressDialog progressDialog;
    SharedPreferences preferences;
    ListView list_city;
    ImageView back,setting,sell_image;
    Spinner spin_qua,spin_pri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Display display = getWindowManager().getDefaultDisplay();
        w = display.getWidth();
        h= display.getHeight();
        setContentView(R.layout.buy_post);
        progressDialog=new ProgressDialog(EditBuyPost.this);
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor=preferences.edit();
        imageLoader=ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(EditBuyPost.this));
        options=new DisplayImageOptions.Builder().showStubImage(R.drawable.images).showImageForEmptyUri(R.drawable.profile_image).cacheOnDisc().cacheInMemory().build();
        desc=(EditText)findViewById(R.id.desc);
        scrollView=(ScrollView)findViewById(R.id.scroll);
        item_name=(EditText)findViewById(R.id.item_name);
        dist=(EditText)findViewById(R.id.dist);
        addme=new ArrayList<>();
        deta=(TextView)findViewById(R.id.deta);
        spin_qua=(Spinner)findViewById(R.id.spin_qua);
        spin_pri=(Spinner)findViewById(R.id.spin_pri);
        sell_city=(RelativeLayout)findViewById(R.id.sell_city);
        list_city=(ListView)findViewById(R.id.list_city);
        phone_nu=(EditText)findViewById(R.id.phone_nu);
        sell_name=(EditText)findViewById(R.id.sell_name);
        address=(EditText)findViewById(R.id.address);
        submit=(Button)findViewById(R.id.submit);
//        sell_price=(EditText)findViewById(R.id.sell_price);
        subcat=(EditText)findViewById(R.id.subcat);
        sell_image=(ImageView)findViewById(R.id.sell_pic);
        back=(ImageView)findViewById(R.id.menu);
        setting=(ImageView)findViewById(R.id.setting);
//        state=(TextView)findViewById(R.id.state);
        choosequan=(EditText)findViewById(R.id.choose_quan);
        city=(TextView)findViewById(R.id.city);
        maxprice=(EditText)findViewById(R.id.maxprice);
        minprice=(EditText)findViewById(R.id.minprice);

        hashme = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("hashme");
        position=getIntent().getExtras().getInt("position");
        id=hashme.get(position).get("sid");
        if (hashme.get(position).get("phone_number").equals("")) {
            phone_nu.setText("");
        } else {
            phone_nu.setText(hashme.get(position).get("phone_number"));
        }
//        try {
        subcat_id = hashme.get(position).get("subcategory_id");
//
            Log.d("subcat_id:", "subcat_id" + subcat_id);
        try {
            String str =hashme.get(position).get("Quantity") ;
             numberOnly = str.replaceAll("[^0-9]", "");
             aplhabetonly = str.replaceAll("[^A-Za-z]+", "");
        }catch (Exception e){

        }
        try {
            String CurrentString = hashme.get(position).get("max");
            String[] separated = CurrentString.split("/");
            Log.d("CurrentString:", "CurrentString" + CurrentString);

            priic = separated[0];
            priun = separated[1];
            Log.d("priic:", "priic" + priic);
        }catch (Exception e){

        }
        if (hashme.get(position).get("Name").equals("")) {
            sell_name.setText("");
        } else {
            sell_name.setText(hashme.get(position).get("Name"));
        }
        if (hashme.get(position).get("category_name").equals("")) {
            item_name.setText("");
        } else {
            item_name.setText(hashme.get(position).get("category_name"));
        }
        if (hashme.get(position).get("district").equals("")) {
            dist.setText("");
        } else {
            dist.setText(hashme.get(position).get("district"));
        }
        if (hashme.get(position).get("Address").equals("")) {
            address.setText("");
        } else {
            address.setText(hashme.get(position).get("Address"));
        }
        if (hashme.get(position).get("Description").equals("")) {
            desc.setText("");
        } else {
            desc.setText(hashme.get(position).get("Description"));
        }
        if (hashme.get(position).get("min").equals("")) {
            minprice.setText("");
        } else {
            minprice.setText(hashme.get(position).get("min"));
        }
        if (hashme.get(position).get("max").equals("")) {
            maxprice.setText("");
        } else {
            maxprice.setText(priic);
        }
        if (hashme.get(position).get("state").equals("")) {
            city.setText("");
        } else {
            city.setText(hashme.get(position).get("state"));
        }
        if (hashme.get(position).get("profile_file").equals("")) {
//            imageLoader.displayImage(hashme.get(position).get("profile_file"), sell_image, options);
        } else {
            imageLoader.displayImage(hashme.get(position).get("profile_file"), sell_image, options);
        }
//        if (hashme.get(position).get("state").equals("")) {
//            city.setText("");
//        } else {
//            city.setText(hashme.get(position).get("state"));
//        }
        if (hashme.get(position).get("item").equals("")) {
            item_name.setText("");
        } else {
            item_name.setText(hashme.get(position).get("item"));
        }
        if (hashme.get(position).get("subcategory_name").equals("")) {
            subcat.setText("");
        } else {
            subcat.setText(hashme.get(position).get("subcategory_name"));
        }

        if (hashme.get(position).get("Quantity").equals("")) {
            choosequan.setText("");
        } else {
            choosequan.setText(numberOnly);
        }
        imageDialog = new Dialog(this);
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = imageDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        imageDialog.setContentView(R.layout.options);
        LinearLayout approx_lay = (LinearLayout) imageDialog.findViewById(R.id.approx_lay);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(w - 30, (h / 3) - 20);
        approx_lay.setLayoutParams(params);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,quan);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<String>adapter1=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,price);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_qua.setAdapter(adapter1);
        spin_pri.setAdapter(adapter);
        spin_qua.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                quan_text = spin_qua.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spin_pri.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pri_text = spin_pri.getSelectedItem().toString();
                Log.d("pri_text","pri_text"+pri_text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sell_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageDialog.show();
            }
        });
//        if(preferences.getString("username","").equals("")){
//            sell_name.setText("");
//        }else{
//            sell_name.setText(preferences.getString("username",""));
//        }
//        if(preferences.getString("adress","").equals("")){
//            address.setText("");
//        }else{
//            address.setText(preferences.getString("adress",""));
//        }
//        if(preferences.getString("dist","").equals("")){
//            dist.setText("");
//        }else{
//            dist.setText(preferences.getString("dist",""));
//        }
//        if(preferences.getString("cname","").equals("")){
//
//        }else{
//            city.setText(preferences.getString("cname",""));
//        }
//        phone_nu.setText(preferences.getString("Phone_Number",""));
//        item_name.setText(cat_name);
//        subcat.setText(sub_name);
        TextView Invite_mail = (TextView) imageDialog.findViewById(R.id.options_camera);
        Invite_mail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//					startActivityForResult(intent, IMAGE_CAMERA_11);

                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, IMAGE_CAMERA_11);
                imageDialog.dismiss();
            }
        });
        TextView Invite_sms = (TextView) imageDialog.findViewById(R.id.options_gallery);
        Invite_sms.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, IMAGE_GALLERY_22);
                imageDialog.dismiss();
            }
        });
        TextView Invite_cancel = (TextView) imageDialog.findViewById(R.id.options_cancel);
        Invite_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                imageDialog.dismiss();
            }
        });

//
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (item_name.getText().toString().length() > 0) {
                    if (choosequan.getText().toString().length() > 0) {
                        if (maxprice.getText().toString().length() > 0) {
                            if (minprice.getText().toString().length() > 0) {
                                maxi=Integer.parseInt(maxprice.getText().toString().trim());
                                mini=Integer.parseInt(minprice.getText().toString().trim());
                                if(maxi>mini) {


//
                                    Addpost();
                                }else{
                                    new ShowMsg().createDialog(EditBuyPost.this, "Minimum price should not greater then maximum price ");
                                }
                            } else {
                                new ShowMsg().createDialog(EditBuyPost.this, "Please enter your min price");
                            }
                        } else {
                            new ShowMsg().createDialog(EditBuyPost.this, "Please enter your max price");
                        }
                    } else {
                        new ShowMsg().createDialog(EditBuyPost.this, "Please enter your quantity");
                    }
                } else {
                    new ShowMsg().createDialog(EditBuyPost.this, "Please enter your item");
                }


            }
        });
        sell_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
                list_city.setVisibility(View.VISIBLE);
                list_city.setAdapter(new CityAdapter(EditBuyPost.this, addme));
            }
        });
        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
                list_city.setVisibility(View.VISIBLE);
                list_city.setAdapter(new CityAdapter(EditBuyPost.this, addme));
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(list_city.getVisibility()==View.VISIBLE ){
                    list_city.setVisibility(View.GONE);
                }
                else{
                    Intent intent=new Intent(EditBuyPost.this,Mybuy.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditBuyPost.this, SettingScrren.class);
                startActivity(intent);
//                finish();
            }
        });
        CityList();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case IMAGE_CAMERA_11:
                if (data != null) {
                    Uri uri = data.getData();
                    try {
                        bitmap = new UserPicture(uri, getContentResolver()).getBitmap();
                        sell_image.setImageBitmap(bitmap);
                    } catch (Exception e) {

                    }
                } else {

                }
                break;
            case IMAGE_GALLERY_22:
                if (data != null) {
                    Uri uri = data.getData();
                    try {
                        bitmap = new UserPicture(uri, getContentResolver()).getBitmap();
                        sell_image.setImageBitmap(bitmap);
                    } catch (Exception e) {

                    }
                } else {

                }
                break;
        }
    }
    //
//
    public void Addpost() {
        url= " http://52.35.22.61/smart_crop/edit_buypost.php?";
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Log.d("url,...... ","url isssss"+url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                Log.d("sss", "sss" + s);
                try {
                    JSONObject jsonObject=new JSONObject(s);
//                    String name = jsonObject.getString("name");
//                    String quantity = jsonObject.getString("quantity");
//                    String price = jsonObject.getString("price");
////                    String quantity = jsonObject.getString("quantity");

                    Intent intent = new Intent(EditBuyPost.this, Mybuy.class);
//                    intent.putExtra("subcat_id", subcat_id);
//                    intent.putExtra("catname", cat_name);
//                    intent.putExtra("sub_cat_name",sub_name);
//                    intent.putExtra("total",total);
                    startActivity(intent);
                    finish();
//                    final Toast toast = Toast.makeText(getApplicationContext(), "Congratulation!your buy post has been published.Please continue to check list of sellers interested in your item", Toast.LENGTH_SHORT);
//                    toast.show();
//
//                    Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            toast.cancel();
//                        }
//                    }, 500);
//                    new ShowMsg().createDialog(Buy_post.this, "Congratulation!your buy post has been published.Please continue to check list of sellers interested in your item");
                    Toast.makeText(EditBuyPost.this, "Congratulation!your buy post has been edit", Toast.LENGTH_LONG).show();

                }catch (Exception e){
                    Log.d("eeeee:", "eeeee" + e);
                }
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
            }


        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                ByteArrayOutputStream arrayOutputStream=new ByteArrayOutputStream();

                if(bitmap==null){

                }else {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, arrayOutputStream);
                    byte[] data = arrayOutputStream.toByteArray();
                    file = Base64.encodeToString(data, Base64.DEFAULT);
                }
                params.put("id", id);
                params.put("user_contact_id", preferences.getString("uid",""));
                params.put("sub_category_id", subcat_id);

                params.put("name", sell_name.getText().toString().trim());
                params.put("item",item_name.getText().toString().trim());

                params.put("quantity", choosequan.getText().toString().trim() + "" + quan_text);
                params.put("price","");
                params.put("address",address.getText().toString().trim());
                params.put("phone",phone_nu.getText().toString().trim());
                params.put("city_id",city.getText().toString().trim());

                params.put("description",desc.getText().toString().trim());
                params.put("min",minprice.getText().toString().trim());
                params.put("max",maxprice.getText().toString().trim()+""+pri_text);
                params.put("subcategory_name",subcat.getText().toString().trim());
                params.put("district",dist.getText().toString().trim());
                params.put("total",""+total);

                if(file==null){
//
                }else {

                    params.put("profile_name", "" + new Date().getTime() + ".png");
                    params.put("profile_file", file);
                }

                return params;
            }
//





//
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
    public void CityList(){
//        progressDialog.setMessage("Loading...");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
        url1="http://52.35.22.61/smart_crop/get_all_cities.php";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("Status");

                    JSONArray jsonArray=jsonObject.getJSONArray("City");
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject City=jsonArray.getJSONObject(i);
                        String id=City.getString("ID");
                        String City_Name=City.getString("City_Name");

                        Log.d("CatName", "CatName" + City_Name);
                        HashMap<String,String> hashMap=new HashMap<>();
                        hashMap.put("City_Name",City_Name);
                        hashMap.put("City_id",id);

                        addme.add(hashMap);
                    }
                    list_city.setAdapter(new CityAdapter(EditBuyPost.this, addme));



                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
//                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class CityAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> addme;
        public CityAdapter(Activity activity,ArrayList<HashMap<String,String>>addme){
            this.activity=activity;
            this.addme=addme;
        }
        @Override
        public int getCount() {
            return addme.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.city_item,null);

            TextView city_name=(TextView)convertView.findViewById(R.id.city_name);
            city_name.setText(addme.get(position).get("City_Name"));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list_city.setVisibility(View.GONE);
                    city.setText(addme.get(position).get("City_Name"));
//                    city_id=addme.get(position).get("City_id");
                }
            });

            return convertView;
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if(list_city.getVisibility()==View.VISIBLE ){
            list_city.setVisibility(View.GONE);
        }
        else{
            Intent intent=new Intent(EditBuyPost.this,Mybuy.class);
            startActivity(intent);
            finish();
        }
    }
}
