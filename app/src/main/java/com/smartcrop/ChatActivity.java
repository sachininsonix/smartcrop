package com.smartcrop;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.Calendar;

/**
 * Created by insonix on 17/11/15.
 */
public class ChatActivity extends Activity implements View.OnClickListener{
    EditText edit_chat;
    ListView chat_list;
    ImageView send;
    String chat_id,Name;
    String sendtext,url,id;
    Calendar cal;
    TextView name,time,price,smartcrop;
    ProgressDialog progressDialog;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
//    static Dialog imageDialog;
    private static final String FIREBASE_URL = "https://smartcrop.firebaseio.com/";
    FirebaseListAdapter<Chat>firebaseListAdapter;
    private String mUsername,photo;
    private Firebase mFirebaseRef;
    private ValueEventListener mConnectedListener;
    String fid,msg,selectedImage_path,imageBase64String;
    ImageView back,setting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);
        name=(TextView)findViewById(R.id.username);
        smartcrop=(TextView)findViewById(R.id.smartcrop);
//        time=(TextView)findViewById(R.id.time);
//        price=(TextView)findViewById(R.id.price);
        edit_chat=(EditText)findViewById(R.id.edit_chat);
        chat_list=(ListView)findViewById(R.id.chat_list_act);
        send=(ImageView)findViewById(R.id.send);
        progressDialog=new ProgressDialog(ChatActivity.this);
        setupUsername();
        Firebase.setAndroidContext(this);
        setTitle("Chatting as " + mUsername);
        try {
            chat_id = getIntent().getExtras().getString("chat_id");
            id = getIntent().getExtras().getString("id");
            msg = getIntent().getExtras().getString("msg");
            Name=getIntent().getExtras().getString("name");
        }catch (Exception e){

        }
//        if(preferences.getString("username","").equals("")){
//            name.setText("");
//        }else{
            name.setText(Name);
//        }
        Log.d("chat_id", "chat_id" + chat_id);
        try {
            mFirebaseRef = new Firebase(FIREBASE_URL).child("chat").child(chat_id);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        back=(ImageView)findViewById(R.id.menu);
        setting=(ImageView)findViewById(R.id.setting);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(ChatActivity.this,HomeCategory.class);
//                startActivity(intent);
                finish();
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChatActivity.this, SettingScrren.class);
                startActivity(intent);
//                finish();
            }
        });
        smartcrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChatActivity.this,HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendMessage();
            }
        });
        firebaseListAdapter = new FirebaseListAdapter<Chat>(mFirebaseRef.limit(50), Chat.class, R.layout.chat_item,R.layout.chat_item_other,  ChatActivity.this, chat_id);
//
        chat_list.setAdapter(firebaseListAdapter);
        firebaseListAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                chat_list.setSelection(firebaseListAdapter.getCount() - 1);
            }
        });

        mConnectedListener = mFirebaseRef.getRoot().child(".info/connected")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        boolean connected = (Boolean) dataSnapshot.getValue();
                        if (connected) {
                        } else {
                        }
                    }
                    @Override
                    public void onCancelled(FirebaseError firebaseError) {
                        // No-op
                    }
                });
    }

    @Override
    public void onClick(View v) {

    }
    private void setupUsername() {
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mUsername = preferences.getString("uid", "");
//        userimage=preferences.getString("photo_path", "");
        Log.d("mUsername", "mUsername" + mUsername);


    }
    private void sendMessage() {

        sendtext = edit_chat.getText().toString();
        Log.d("sendtext","sendtext"+sendtext);
        if (!sendtext.equals("")) {
            // Create our 'model', a Chat object
            Chat chat = new Chat(sendtext, mUsername,getTime(),"message");
            // Create a new, auto-generated child of that chat location, and save our chat data there
            mFirebaseRef.push().setValue(chat);
            Sendchat();
//            new ChatTask().execute(sendtext);
            edit_chat.setText("");
        }
    }
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        mFirebaseRef.getRoot().child(".info/connected")
                .removeEventListener(mConnectedListener);
        firebaseListAdapter.cleanup();
    }

    public void Sendchat() {

//
        url="http://52.35.22.61/smart_crop/notification.php?sender_id="+preferences.getString("uid","")+"&receiver_id="+id+"&message="+ URLEncoder.encode(sendtext);
        Log.d("urlis", "device url " + url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:","resss"+jsonObject.toString());
                try{
//

                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
//                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:", "volleyError" + volleyError);
//                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    private String getTime(){
        String hour_st;
        String minute_st;
        String am_pm_st;
        String month_st = null;
        String date_st;
        cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minutes = cal.get(Calendar.MINUTE);
        int month = cal.get(Calendar.MONTH);
        int date = cal.get(Calendar.DATE);
        int am_pm=cal.get(Calendar.AM_PM);
        int year=cal.get(Calendar.YEAR);
        if(am_pm==0){
            am_pm_st="AM";
        }else{
            am_pm_st="PM";
        }
        if (hour < 10) {
            hour_st = "0" + hour;
        } else {
            hour_st = "" + hour;
        }
        if (minutes < 10) {

            minute_st = "0" + minutes;
        } else {
            minute_st = "" + minutes;
        }
		/*if (month < 10) {

			month_st = "0" + month;
		} else {
			month_st = "" + minutes;
		}*/
        if (date < 10) {
            date_st = "0" + date;
        } else {
            date_st = "" + date;
        }
        switch (month) {
            case 0:
                month_st="Jan";
                break;
            case 1:
                month_st="Feb";
                break;
            case 2:
                month_st="March";
                break;
            case 3:
                month_st="April";
                break;

            case 4:
                month_st="May";
                break;

            case 5:
                month_st="June";
                break;

            case 6:
                month_st="July";
                break;

            case 7:
                month_st="Aug";
                break;

            case 8:
                month_st="Sept";
                break;
            case 9:
                month_st="Oct";
                break;
            case 10:
                month_st="Nov";
                break;
            case 11:
                month_st="Dec";
                break;
        }
		/*String time = hour_st + ":" + minute_st + "\n"
				+date_st+"/"+month_st + "/" + cal.get(Calendar.YEAR);*/
//        String time = date + " " + month_st + ", "
//                +hour_st+":"+minute_st+" "+am_pm_st + " "+year ;
        String time = hour_st+":"+minute_st+" "+am_pm_st + " "+date + " " + month_st + ", "
                +year ;
        Log.d("Time:", "Time" + time);
        return time;

    }
}
