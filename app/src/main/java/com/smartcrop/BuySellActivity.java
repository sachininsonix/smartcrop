package com.smartcrop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by insonix on 10/11/15.
 */
public class BuySellActivity extends Activity {
    Button buy,sell;
    String subcat_id,cat_name,sub_name;
    ImageView back,setting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sell_buy);
        sell=(Button)findViewById(R.id.sell);
        buy=(Button)findViewById(R.id.buy);

        back=(ImageView)findViewById(R.id.menu);
        setting=(ImageView)findViewById(R.id.setting);
        subcat_id=getIntent().getExtras().getString("subcat_id");
        cat_name=getIntent().getExtras().getString("catname");
        sub_name=getIntent().getExtras().getString("sub_cat_name");
        Log.d("subcat_id:", "subcat_id" + subcat_id);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(BuySellActivity.this,HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(BuySellActivity.this,SettingScrren.class);
                startActivity(intent);
//                finish();
            }
        });
        sell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(BuySellActivity.this,SellPost.class);
                intent.putExtra("subcat_id",subcat_id);
                intent.putExtra("catname",cat_name);
                intent.putExtra("sub_cat_name",sub_name);
                startActivity(intent);

            }
        });
        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(BuySellActivity.this,SellingList.class);
                intent.putExtra("subcat_id",subcat_id);
                intent.putExtra("catname",cat_name);
                intent.putExtra("sub_cat_name",sub_name);
                startActivity(intent);
            }
        });
    }
}
