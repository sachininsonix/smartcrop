package com.smartcrop;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by insonix on 17/11/15.
 */
public class MyAccount extends Activity implements View.OnClickListener {
    ImageView edit,sell_image,online;
    TextView name,time,phone,smartcrop;
    RelativeLayout rel_sell,rel_chat,rel_buy;




    String url;
    ImageLoader imageLoader;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    DisplayImageOptions options;
    ImageView back,setting;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_account);

        progressDialog=new ProgressDialog(MyAccount.this);
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor=preferences.edit();
        imageLoader=ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(MyAccount.this));
        options=new DisplayImageOptions.Builder().showStubImage(R.drawable.images).showImageForEmptyUri(R.drawable.profile_image).cacheOnDisc().cacheInMemory().build();
        sell_image=(ImageView)findViewById(R.id.sell_image);
        edit=(ImageView)findViewById(R.id.edit);
        edit.setOnClickListener(this);
        online=(ImageView)findViewById(R.id.online);
        phone=(TextView)findViewById(R.id.phone);
        name=(TextView)findViewById(R.id.name);
        time=(TextView)findViewById(R.id.time);
        smartcrop=(TextView)findViewById(R.id.smartcrop);

        rel_chat=(RelativeLayout)findViewById(R.id.rel_chat);
        rel_chat.setOnClickListener(this);
        rel_sell=(RelativeLayout)findViewById(R.id.rel_sell);
        rel_sell.setOnClickListener(this);

        rel_buy=(RelativeLayout)findViewById(R.id.rel_buy);
        rel_buy.setOnClickListener(this);





        back=(ImageView)findViewById(R.id.menu);
        setting=(ImageView)findViewById(R.id.setting);
        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
//                .setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);
//        AdView mAdView = (AdView) findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

        if(preferences.getString("username","").equals("")){
    name.setText("");
}else{
    name.setText(preferences.getString("username",""));
}
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccount.this, SettingScrren.class);
                startActivity(intent);
                finish();
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccount.this, SettingScrren.class);
                startActivity(intent);
//                finish();
            }
        });
        smartcrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccount.this, HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });


        ProfileTask();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.rel_chat:
                Intent chat=new Intent(MyAccount.this,ChatHistory.class);
                startActivity(chat);
                finish();
                break;
            case R.id.rel_sell:
                Intent sell=new Intent(MyAccount.this,Mysell.class);
                startActivity(sell);
                finish();
                break;
            case R.id.rel_buy:
                Intent buy=new Intent(MyAccount.this,MywishList.class);
                startActivity(buy);
                finish();
                break;
            case R.id.edit:
                Intent intent = new Intent(MyAccount.this, Editprofile.class);
                startActivity(intent);
                finish();

                break;
        }
    }
    public void ProfileTask(){
        url="http://52.35.22.61/smart_crop/view_user_contact.php?id="+preferences.getString("uid","");
        Log.d("","hellll "+url);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject jsonObject) {
                try{
                    Log.d("","jsonObject "+jsonObject.toString());
                    String Status=jsonObject.getString("Status");
                    if(Status.equalsIgnoreCase("OK")){
                        JSONObject detail=jsonObject.getJSONObject("user_contact");
//                        String uid=detail.getString("uid");
                        String Name=detail.getString("Name");

                        String Address=detail.getString("Address");
              String phone_number=detail.getString("phone_number");
                       String  photo_path=detail.getString("profile_file");


                        name.setText(Name);
                        time.setText(Address);
                        phone.setText(phone_number);




                            imageLoader.displayImage(photo_path, sell_image, options);
                          editor.putString("photo_path", photo_path);
                        editor.putString("Address",Address);
                        editor.putString("phone_number",phone_number);
                        editor.putString("username",Name);
                        editor.commit();

//

                    }
                }catch(Exception e){
                    Log.d("","exxxx "+e);
                }
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
            }

        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(MyAccount.this, SettingScrren.class);
//        startActivity(intent);
        finish();
    }
}
