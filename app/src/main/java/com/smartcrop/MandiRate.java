package com.smartcrop;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by insonix on 15/12/15.
 */
public class MandiRate extends Activity {
    ImageView back,setting,search;
    ArrayList<HashMap<String,String >> addme;
    ArrayList<HashMap<String,String >> city_list;
    ProgressDialog progressDialog;
    String url,url1,url2,lang,url4;
    ListView mandi_list,list_city;
    TextView set,state,search_state,smartcrop;
    ListView category;
    LinearLayout stat,cat;
    EditText search_category;
    InputMethodManager imm;
    SharedPreferences preferences;
    SharedPreferences.Editor editor ;
    SellListAdapter sellListAdapter;
    CategoryAdapter categoryAdapter;
    ArrayList<HashMap<String,String >>searchlist;
    ArrayList<HashMap<String,String >>list_search;
    ArrayList<HashMap<String,String >>searchcategorylist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mandirate);
        progressDialog=new ProgressDialog(MandiRate.this);
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        back=(ImageView)findViewById(R.id.menu);
        setting=(ImageView)findViewById(R.id.setting);
        search=(ImageView)findViewById(R.id.search);
        set=(TextView)findViewById(R.id.set);
        smartcrop=(TextView)findViewById(R.id.smartcrop);
        category=(ListView)findViewById(R.id.category);
        search=(ImageView)findViewById(R.id.search);
        list_city=(ListView)findViewById(R.id.statelist);
        search_state=(TextView)findViewById(R.id.search_state);
        stat=(LinearLayout)findViewById(R.id.stat);
        cat=(LinearLayout)findViewById(R.id.cat);
        mandi_list=(ListView)findViewById(R.id.mandilist);

        addme=new ArrayList<>();
        list_search=new ArrayList<>();
        searchlist = new ArrayList<>();
        searchcategorylist = new ArrayList<>();
        city_list = new ArrayList<>();
        search_category=(EditText)findViewById(R.id.search_category);
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());
        set.setText(formattedDate);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(category.getVisibility()==View.VISIBLE ){
                    category.setVisibility(View.GONE);
                }
                else if(list_city.getVisibility()==View.VISIBLE){
                    list_city.setVisibility(View.GONE);
                } else{
                    Intent intent = new Intent(MandiRate.this, HomeCategory.class);
                    startActivity(intent);
                    finish();
                }

            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MandiRate.this,SettingScrren.class);
                startActivity(intent);
//                finish();
            }
        });
        smartcrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MandiRate.this, HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });
        if(preferences.getString("on","").equals("1")){
            lang="1";
//
        }else if(preferences.getString("on","").equals("2")){
            lang="2";
//
        }else{
            lang="0";
        }
        cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category.setVisibility(View.VISIBLE);
//                category.setAdapter(new CategoryAdapter(MandiRate.this, searchcategorylist));
                searchcategorylist = new ArrayList<>(addme);
                categoryAdapter = new CategoryAdapter(MandiRate.this, searchcategorylist);
                category.setAdapter(categoryAdapter);
            }
        });
        stat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list_city.setVisibility(View.VISIBLE);
                list_city.setAdapter(new CityAdapter(MandiRate.this, city_list));
            }
        });
        search_category.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                category.setVisibility(View.VISIBLE);
                imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(search_category.getWindowToken(), 0);

                String searchString = search_category.getText().toString();
                Log.d("searchString", "searchString" + searchString);
                int textLength = searchString.length();
                searchcategorylist.clear();
                for (int i = 0; i < addme.size(); i++) {
                    String catename = addme.get(i).get("catname").toString();
//                        String catd = addme.get(i).get("catid").toString();
                    Log.d("categoryname", "categoryname" + catename);
                    if (textLength <= catename.length()) {

                        if (searchString.equalsIgnoreCase(catename.substring(0, textLength)))
                            searchcategorylist.add(addme.get(i));
//                       Log.d("Added","Added"+addme.get(i));
                    }
                }
//                sub_category.setAdapter(subCategoryAdapter);
                categoryAdapter.notifyDataSetChanged();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
//        search_category.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                String searchString = search_category.getText().toString();
//                int textLength = searchString.length();
//                searchlist.clear();
//                for (int i = 0; i < addme.size(); i++) {
//                    String catename = addme.get(i).get("state").toString();
//                    if (textLength <= catename.length()) {
//                        if (searchString.equalsIgnoreCase(catename.substring(0, textLength)))
//                            searchlist.add(addme.get(i));
//
//                    }
//                }
//                Collections.sort(searchlist, mapComparator);
////                sub_category.setAdapter(subCategoryAdapter);
//                sellListAdapter.notifyDataSetChanged();
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
        MandiList();
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(search_state.getText().toString().trim().length()>0&&(search_category.getText().toString().trim().length()>0)){
                    SearchList();
                }else{
                    Toast.makeText(MandiRate.this,"Please fill both fields",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    public void MandiList(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url="http://52.35.22.61/smart_crop/get_allrates.php";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("Status");
                    if(status.equalsIgnoreCase("OK")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("crop");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject City = jsonArray.getJSONObject(i);
//                            String id = City.getString("User_Contact_ID");
                            String item = City.getString("category");
                            String min_price = City.getString("min_price");
                            String max_price = City.getString("max_price");
                            String state = City.getString("state");
                            String district = City.getString("district");
                            String market = City.getString("market");
//                            String phone_number = City.getString("phone_number");
//                            String chat_id = City.getString("chat_id");
//                            String profile_image = City.getString("profile_file");

                            Log.d("CatName", "CatName" + item);
                            HashMap<String, String> hashMap = new HashMap<>();
//                            hashMap.put("id", id);
                            hashMap.put("min_price", min_price);
                            hashMap.put("item", item);
                            hashMap.put("max_price", max_price);
                            hashMap.put("state", state);
                            hashMap.put("district", district);
                            hashMap.put("market", market);
//                            hashMap.put("phone_number", phone_number);
//                            hashMap.put("chat_id", chat_id);
//                            hashMap.put("profile_file", profile_image);


                            searchlist.add(hashMap);
//                            for(int k=1;k<addme.size();k++){
//                                String state1=addme.get(k).get("state")
//                            }

                        }
//
                        AllCategoryList();
                        sellListAdapter = new SellListAdapter(MandiRate.this, searchlist);
                        mandi_list.setAdapter(sellListAdapter);
//
                        Collections.sort(searchlist, mapComparator);

                    }else{
                        mandi_list.setVisibility(View.GONE);
                        new ShowMsg().createDialog(MandiRate.this,"Oops! No mandi rate show");
                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }

    public void SearchList(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url4="http://52.35.22.61/smart_crop/search_allrates.php?state="+URLEncoder.encode(search_state.getText().toString().trim())+"&cat="+URLEncoder.encode(search_category.getText().toString().trim());
        Log.d("url4","url4"+url4);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url4, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                searchlist.clear();
                try{
                    String status=jsonObject.getString("Status");
                    if(status.equalsIgnoreCase("OK")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("crop");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject City = jsonArray.getJSONObject(i);
//                            String id = City.getString("User_Contact_ID");
                            String item = City.getString("category");
                            String min_price = City.getString("min_price");
                            String max_price = City.getString("max_price");
                            String state = City.getString("state");
                            String district = City.getString("district");
                            String market = City.getString("market");
//                            String phone_number = City.getString("phone_number");
//                            String chat_id = City.getString("chat_id");
//                            String profile_image = City.getString("profile_file");

                            Log.d("CatName", "CatName" + item);
                            HashMap<String, String> hashMap = new HashMap<>();
//                            hashMap.put("id", id);
                            hashMap.put("min_price", min_price);
                            hashMap.put("item", item);
                            hashMap.put("max_price", max_price);
                            hashMap.put("state", state);
                            hashMap.put("district", district);
                            hashMap.put("market", market);
//                            hashMap.put("phone_number", phone_number);
//                            hashMap.put("chat_id", chat_id);
//                            hashMap.put("profile_file", profile_image);


                            list_search.add(hashMap);
//                            for(int k=1;k<addme.size();k++){
//                                String state1=addme.get(k).get("state")
//                            }

                        }
//

                        sellListAdapter = new SellListAdapter(MandiRate.this, list_search);
                        mandi_list.setAdapter(sellListAdapter);
//
                        Collections.sort(list_search, mapComparator);

                    }else{
                        mandi_list.setVisibility(View.GONE);
                        new ShowMsg().createDialog(MandiRate.this,"Oops! No mandi rate show");

                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class SellListAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> searchlist;
        public SellListAdapter(Activity activity,ArrayList<HashMap<String,String>>searchlist){
            this.activity=activity;
            this.searchlist=searchlist;
        }
        @Override
        public int getCount() {
            return searchlist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.mandi_item,null);

            TextView max=(TextView)convertView.findViewById(R.id.max);
            TextView name=(TextView)convertView.findViewById(R.id.name);
            TextView min=(TextView)convertView.findViewById(R.id.min);
            TextView statename=(TextView)convertView.findViewById(R.id.statename);
            TextView dist=(TextView)convertView.findViewById(R.id.dist);
//            ImageView chat=(ImageView)convertView.findViewById(R.id.chat);
//            ImageView sell_image=(ImageView)convertView.findViewById(R.id.sell_image);
//
//            ImageView online=(ImageView)convertView.findViewById(R.id.online);
            TextView market=(TextView)convertView.findViewById(R.id.market);

            name.setText(searchlist.get(position).get("item"));
            max.setText(searchlist.get(position).get("max_price"));
            min.setText(searchlist.get(position).get("min_price"));
            statename.setText(searchlist.get(position).get("state"));
            dist.setText(searchlist.get(position).get("district"));
            market.setText(searchlist.get(position).get("market"));





//

            return convertView;
        }
    }
    public void AllCategoryList(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url1="http://52.35.22.61/smart_crop/get_all_categories.php?city_id="+ URLEncoder.encode(preferences.getString("cname",""))+"&lang="+lang;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("Status");
                    if(status.equalsIgnoreCase("OK")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("category");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject catlist = jsonArray.getJSONObject(i);
                            String id = catlist.getString("ID");
                            String CatName = catlist.getString("CatName");

                            Log.d("CatName", "CatName" + CatName);
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("catname", CatName);

                            hashMap.put("catid", id);
                            addme.add(hashMap);
                        }
                        searchcategorylist = new ArrayList<>(addme);
                        categoryAdapter = new CategoryAdapter(MandiRate.this, searchcategorylist);
                        category.setAdapter(categoryAdapter);
                        CityList();
//                        category.setAdapter(new CategoryAdapter(MandiRate.this, addme));
                    }else{
                        new ShowMsg().createDialog(MandiRate.this, "No record found");

                    }
                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class CategoryAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> searchcategorylist;
        public CategoryAdapter(Activity activity,ArrayList<HashMap<String,String>>searchcategorylist){
            this.activity=activity;
            this.searchcategorylist=searchcategorylist;
        }
        @Override
        public int getCount() {
            return searchcategorylist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.city_item,null);
//            ImageView cat_image=(ImageView)convertView.findViewById(R.id.cat_image);
            TextView cat_name=(TextView)convertView.findViewById(R.id.city_name);
            RelativeLayout rel_help=(RelativeLayout)convertView.findViewById(R.id.rel_help);
            cat_name.setText(searchcategorylist.get(position).get("catname"));
//            cat_name.setTypeface(font);
//            rel_help.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(activity, SubCategory.class);
//                    intent.putExtra("catname", addme.get(position).get("catname"));
//                    intent.putExtra("catid", addme.get(position).get("catid"));
//                    intent.putExtra("city_id",city_id);
//                    activity.startActivity(intent);
//                }
//            });
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    category.setVisibility(View.GONE);
                    search_category.setText(searchcategorylist.get(position).get("catname"));
//                    city_id=addme.get(position).get("City_id");
                }
            });

            return convertView;
        }
    }
    public Comparator<Map<String, String>> mapComparator = new Comparator<Map<String, String>>() {
        public int compare(Map<String, String> m1, Map<String, String> m2) {
            return m1.get("state").compareTo(m2.get("state"));
        }
    };
    public void CityList(){
//        progressDialog.setMessage("Loading...");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
        url2="http://52.35.22.61/smart_crop/get_all_cities.php";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url2, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("Status");

                    JSONArray jsonArray=jsonObject.getJSONArray("City");
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject City=jsonArray.getJSONObject(i);
                        String id=City.getString("ID");
                        String City_Name=City.getString("City_Name");

                        Log.d("CatName", "CatName" + City_Name);
                        HashMap<String,String> hashMap=new HashMap<>();
                        hashMap.put("City_Name",City_Name);
                        hashMap.put("City_id",id);

                        city_list.add(hashMap);
                    }
                    list_city.setAdapter(new CityAdapter(MandiRate.this, city_list));



                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
//                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class CityAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> city_list;
        public CityAdapter(Activity activity,ArrayList<HashMap<String,String>>city_list){
            this.activity=activity;
            this.city_list=city_list;
        }
        @Override
        public int getCount() {
            return city_list.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.city_item,null);

            final TextView city_name=(TextView)convertView.findViewById(R.id.city_name);
//            final CheckBox checkBox=(CheckBox)convertView.findViewById(R.id.chckbox);
            city_name.setText(city_list.get(position).get("City_Name"));
//            city_name.setText(city_list.get(position).get("City_Name"));
//            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    if(checkBox.isChecked()) {
//                        statechecked.add(city_name.getText().toString());
//                        state.setText(city_name.getText().toString());
////                        stttate =statechecked.toString();
//
////                        Toast.makeText(getApplicationContext(),
////                                "Clicked on Checkbox: " + city_name.getText().toString()
////                                       ,
////                                Toast.LENGTH_LONG).show();
//                    }
//                }
//            });
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list_city.setVisibility(View.GONE);
                    search_state.setText(city_list.get(position).get("City_Name"));
//                    city_id=addme.get(position).get("City_id");
                }
            });
//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    list_city.setVisibility(View.GONE);
//                    city.setText(city_list.get(position).get("City_Name"));
////                    city_id=addme.get(position).get("City_id");
//                }
//            });

            return convertView;
        }
    }
    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if(category.getVisibility()==View.VISIBLE ){
            category.setVisibility(View.GONE);
        }
        else if(list_city.getVisibility()==View.VISIBLE){
            list_city.setVisibility(View.GONE);
        } else{
            Intent intent = new Intent(MandiRate.this, HomeCategory.class);
            startActivity(intent);
            finish();
        }
    }
}
