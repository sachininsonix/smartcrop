package com.smartcrop;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by insonix on 19/1/16.
 */
public class EditWishList extends Activity {
    TextView state,smartcrop;
    ListView list_city;
    ListView category;
    int position;
    RelativeLayout rel_all,sell_city;
    String url,lang,url1,cat_id,url2,id,priic,priun;
    SharedPreferences preferences;
    SharedPreferences.Editor editor ;
    ProgressDialog progressDialog;
    Button submit,done;
    ArrayList<HashMap<String,String>>hashme;
    String stttate;
    EditText minprice,maxprice,cityedit;
    ArrayList<String > statechecked;
    ArrayList<HashMap<String,String >>city_list;
    ArrayList<HashMap<String,String >>searchlist;
    CategoryAdapter categoryAdapter;
    InputMethodManager imm;
    Spinner spin_qua;
    int maxi,mini;
    ImageView back,setting;
    String[]quan={"/kg","/gm","/ton"};
    ArrayList<HashMap<String,String >>addme;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_wish);
        progressDialog=new ProgressDialog(EditWishList.this);
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cityedit=(EditText)findViewById(R.id.editcity);
        sell_city=(RelativeLayout)findViewById(R.id.sell_city);
        state=(TextView)findViewById(R.id.state);
        smartcrop=(TextView)findViewById(R.id.smartcrop);
        submit=(Button)findViewById(R.id.submit);
        done=(Button)findViewById(R.id.done);
        maxprice=(EditText)findViewById(R.id.maxprice);
        minprice=(EditText)findViewById(R.id.minprice);
        back=(ImageView)findViewById(R.id.menu);
        setting=(ImageView)findViewById(R.id.setting);
        addme=new ArrayList<>();
        statechecked=new ArrayList<>();
        rel_all=(RelativeLayout)findViewById(R.id.rel_all);
        list_city=(ListView)findViewById(R.id.statelist);
        city_list=new ArrayList<>();
        spin_qua=(Spinner)findViewById(R.id.spin_qua);
        category=(ListView)findViewById(R.id.wishlist);
        hashme = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("hashme");
        position=getIntent().getExtras().getInt("position");
        id=hashme.get(position).get("id");
        cat_id=hashme.get(position).get("cat_id");
        try {
            String CurrentString = hashme.get(position).get("max");
            String[] separated = CurrentString.split("/");
            Log.d("CurrentString:", "CurrentString" + CurrentString);

            priic = separated[0];
            priun = separated[1];
            Log.d("priic:", "priic" + priic);
        }catch (Exception e){

        }
        if (hashme.get(position).get("min").equals("")) {
            minprice.setText("");
        } else {
            minprice.setText(hashme.get(position).get("min"));
        }
        if (hashme.get(position).get("max").equals("")) {
            maxprice.setText("");
        } else {
            maxprice.setText(priic);
        }
        if (hashme.get(position).get("state").equals("")) {
            state.setText("");
        } else {
            state.setText(hashme.get(position).get("state"));
        }
        if (hashme.get(position).get("CatName").equals("")) {
            cityedit.setText("");
        } else {
            cityedit.setText(hashme.get(position).get("CatName"));
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(category.getVisibility()==View.VISIBLE ){
                    category.setVisibility(View.GONE);
                }
                else if(list_city.getVisibility()==View.VISIBLE){
                    list_city.setVisibility(View.GONE);
                } else{
                    Intent intent = new Intent(EditWishList.this, MywishList.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditWishList.this, SettingScrren.class);
                startActivity(intent);
//                finish();
            }
        });
        smartcrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditWishList.this, HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });

        state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list_city.setVisibility(View.VISIBLE);
                done.setVisibility(View.VISIBLE);
                rel_all.setVisibility(View.GONE);
                list_city.setAdapter(new CityAdapter(EditWishList.this, city_list));
            }
        });
        sell_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                city.setVisibility(View.VISIBLE);


                category.setVisibility(View.VISIBLE);
                searchlist = new ArrayList<>(addme);
                categoryAdapter = new CategoryAdapter(EditWishList.this, searchlist);
                category.setAdapter(categoryAdapter);


            }
        });
        cityedit.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                category.setVisibility(View.VISIBLE);
                imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(cityedit.getWindowToken(), 0);

                String searchString = cityedit.getText().toString();
                Log.d("searchString", "searchString" + searchString);
                int textLength = searchString.length();
                searchlist.clear();
                for (int i = 0; i < addme.size(); i++) {
                    String catename = addme.get(i).get("catname").toString();
//                        String catd = addme.get(i).get("catid").toString();
                    Log.d("categoryname", "categoryname" + catename);
                    if (textLength <= catename.length()) {

                        if (searchString.equalsIgnoreCase(catename.substring(0, textLength)))
                            searchlist.add(addme.get(i));
//                       Log.d("Added","Added"+addme.get(i));
                    }
                }
//                sub_category.setAdapter(subCategoryAdapter);
                categoryAdapter.notifyDataSetChanged();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        if(preferences.getString("on","").equals("1")){
            lang="1";
//
        }else if(preferences.getString("on","").equals("2")){
            lang="2";
//
        }else{
            lang="0";
        }
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,quan);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_qua.setAdapter(adapter);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (maxprice.getText().toString().length() > 0) {
                    if (minprice.getText().toString().length() > 0) {
                        maxi=Integer.parseInt(maxprice.getText().toString().trim());
                        mini=Integer.parseInt(minprice.getText().toString().trim());
                        if(maxi>mini) {
                        Addwish();
                        }else{
                            new ShowMsg().createDialog(EditWishList.this, "Minimum price should not greater then maximum price ");
                        }
                    } else {
                        new ShowMsg().createDialog(EditWishList.this, "Please enter your min price");
                    }
                } else {
                    new ShowMsg().createDialog(EditWishList.this, "Please enter your max price");
                }
            }
        });
//        category.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                category.setVisibility(View.GONE);
////
////                imm.hideSoftInputFromWindow(city_name_c.getWindowToken(), 0);
//
////                editor.putString("cname", city_name_c.getText().toString().trim());
//////                editor.putString("cname",city_listing.get(position).get("City_Name"));
////
////                editor.commit();
//                cityedit.setText(addme.get(position).get("catname"));
////
//                cat_id = addme.get(position).get("catid");
////                Log.d("ccc", "ccc" + city_id);
////                url = "http://demo.insonix.com/smart_crop/get_main_categories.php?city_id="+city_id+"&lang=0";
//
//            }
//        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stttate=statechecked.toString().replace("[", "").replace("]", "").replace(", ", ",").trim();
                state.setText(stttate);
                list_city.setVisibility(View.GONE);
                done.setVisibility(View.GONE );
                rel_all.setVisibility(View.VISIBLE);
            }
        });
        AllCategoryList();

    }
    public void AllCategoryList(){

        progressDialog.setCancelable(false);
        progressDialog.show();
        url="http://52.35.22.61/smart_crop/get_all_categories.php?city_id="+ URLEncoder.encode(preferences.getString("cname", ""))+"&lang="+lang;
        Log.d("urlurlurl", "url" + url);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("Status");
                    if(status.equalsIgnoreCase("OK")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("category");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject catlist = jsonArray.getJSONObject(i);
                            String id = catlist.getString("ID");
                            String CatName = catlist.getString("CatName");

                            Log.d("CatName", "CatName" + CatName);
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("catname", CatName);

                            hashMap.put("catid", id);
                            addme.add(hashMap);
                        }
                        searchlist = new ArrayList<>(addme);
                        categoryAdapter = new CategoryAdapter(EditWishList.this, searchlist);
                        category.setAdapter(categoryAdapter);
                        CityList();
//                        category.setAdapter(new CategoryAdapter(EditWishList.this, addme));

                    }else{
                        new ShowMsg().createDialog(EditWishList.this, "No record found");

                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    public void Addwish(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url1="http://52.35.22.61/smart_crop/edit_wishlist.php?id="+id+"&category_id="+URLEncoder.encode(cityedit.getText().toString())+"&max="+maxprice.getText().toString().trim()+"&min="+minprice.getText().toString().trim()+"&state="+URLEncoder.encode(state.getText().toString());
        Log.d("url1url1url1","url1"+url1);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());

                Intent intent=new Intent(EditWishList.this,MywishList.class);
                startActivity(intent);
                finish();

                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class CategoryAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> searchlist;
        public CategoryAdapter(Activity activity,ArrayList<HashMap<String,String>>searchlist){
            this.activity=activity;
            this.searchlist=searchlist;
        }
        @Override
        public int getCount() {
            return searchlist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.city_item,null);
//            ImageView cat_image=(ImageView)convertView.findViewById(R.id.cat_image);
            TextView cat_name=(TextView)convertView.findViewById(R.id.city_name);
            RelativeLayout rel_help=(RelativeLayout)convertView.findViewById(R.id.rel_help);
            cat_name.setText(searchlist.get(position).get("catname"));
//            cat_name.setTypeface(font);
//            rel_help.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(activity, SubCategory.class);
//                    intent.putExtra("catname", addme.get(position).get("catname"));
//                    intent.putExtra("catid", addme.get(position).get("catid"));
//                    intent.putExtra("city_id",city_id);
//                    activity.startActivity(intent);
//                }
//            });
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    category.setVisibility(View.GONE);
//
                    cityedit.setText(searchlist.get(position).get("catname"));
//                    cat_id = searchlist.get(position).get("catid");
//                    Log.d("aaann","caaa"+cat_id);
                }
            });


            return convertView;
        }
    }
    public void CityList(){
//        progressDialog.setMessage("Loading...");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
        url2="http://52.35.22.61/smart_crop/get_all_cities.php";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url2, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("Status");

                    JSONArray jsonArray=jsonObject.getJSONArray("City");
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject City=jsonArray.getJSONObject(i);
                        String id=City.getString("ID");
                        String City_Name=City.getString("City_Name");

                        Log.d("CatName", "CatName" + City_Name);
                        HashMap<String,String> hashMap=new HashMap<>();
                        hashMap.put("City_Name",City_Name);
                        hashMap.put("City_id",id);

                        city_list.add(hashMap);
                    }
                    list_city.setAdapter(new CityAdapter(EditWishList.this, city_list));



                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
//                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class CityAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> city_list;
        public CityAdapter(Activity activity,ArrayList<HashMap<String,String>>city_list){
            this.activity=activity;
            this.city_list=city_list;
        }
        @Override
        public int getCount() {
            return city_list.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.checkbox_detail,null);

            final TextView city_name=(TextView)convertView.findViewById(R.id.city_name);
            final CheckBox checkBox=(CheckBox)convertView.findViewById(R.id.chckbox);
            city_name.setText(city_list.get(position).get("City_Name"));
            state.setText(city_list.get(position).get("City_Name"));
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(checkBox.isChecked()) {
                        statechecked.add(city_name.getText().toString());
//                        state.setText(city_name.getText().toString());
//                        stttate =statechecked.toString();

//                        Toast.makeText(getApplicationContext(),
//                                "Clicked on Checkbox: " + city_name.getText().toString()
//                                       ,
//                                Toast.LENGTH_LONG).show();
                    }
                }
            });
//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    list_city.setVisibility(View.GONE);
//                    city.setText(city_list.get(position).get("City_Name"));
////                    city_id=addme.get(position).get("City_id");
//                }
//            });

            return convertView;
        }
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if(category.getVisibility()==View.VISIBLE ){
            category.setVisibility(View.GONE);
        }
        else if(list_city.getVisibility()==View.VISIBLE){
            list_city.setVisibility(View.GONE);
        } else{
            Intent intent = new Intent(EditWishList.this, MywishList.class);
            startActivity(intent);
            finish();
        }
    }

}
