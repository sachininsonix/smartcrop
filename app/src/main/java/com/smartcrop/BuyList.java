package com.smartcrop;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by insonix on 17/12/15.
 */
public class BuyList extends Activity {
    Button buy;
    ListView sell_list;
    ImageView add,sell;
    TextView set,smartcrop;
    ArrayList<HashMap<String,String >> addme;
    ProgressDialog progressDialog;
    String subcat_id,url1,cat_name,sub_name,Namecategory;
    double total;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    SharedPreferences preferences;
    ImageView back,setting,filter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buylist);
        sell_list=(ListView)findViewById(R.id.sell_list);
        sell=(ImageView)findViewById(R.id.sell);
        buy=(Button)findViewById(R.id.buy);
        filter=(ImageView)findViewById(R.id.filter);
        add=(ImageView)findViewById(R.id.add);
        set=(TextView)findViewById(R.id.set);
        smartcrop=(TextView)findViewById(R.id.smartcrop);
        progressDialog=new ProgressDialog(BuyList.this);
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        imageLoader=ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(BuyList.this));
        options=new DisplayImageOptions.Builder().showStubImage(R.drawable.images).showImageForEmptyUri(R.drawable.profile_image).cacheOnDisc().cacheInMemory().build();
        addme=new ArrayList<>();
        try {
            subcat_id = getIntent().getExtras().getString("subcat_id");
            cat_name = getIntent().getExtras().getString("catname");
            sub_name = getIntent().getExtras().getString("sub_cat_name");

//            total = getIntent().getExtras().getDouble("total");
            Log.d("subcat_id:", "subcat_id" + subcat_id);
        }catch (Exception e){

        }
//        set.setText("List of Buyers");
//       buy.setText("View Sellers");
//        sell.setText("Want to buy");
//        sell.setBackgroundColor(Color.parseColor("#b27c00"));
        back=(ImageView)findViewById(R.id.menu);
        setting=(ImageView)findViewById(R.id.setting);

//        add.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(BuyList.this,Buy_post.class);
//                intent.putExtra("subcat_id",subcat_id);
//                intent.putExtra("catname",cat_name);
//                intent.putExtra("sub_cat_name",sub_name);
//                startActivity(intent);
//                finish();
//            }
//        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(BuyList.this,HomeCategory.class);
//                intent.putExtra("subcat_id",subcat_id);
                startActivity(intent);
                finish();
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuyList.this, SettingScrren.class);
                startActivity(intent);
//                finish();
            }
        });
        smartcrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuyList.this,HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });
        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(BuyList.this,SellingList.class);
                intent.putExtra("subcat_id",subcat_id);
                intent.putExtra("catname",cat_name);
                intent.putExtra("sub_cat_name", sub_name);
                intent.putExtra("total", total);
                startActivity(intent);
//                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                finish();
                overridePendingTransition(0, 0);

            }
        });
        sell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(BuyList.this,Buy_post.class);
                intent.putExtra("subcat_id",subcat_id);
                intent.putExtra("catname",cat_name);
                intent.putExtra("sub_cat_name",sub_name);
                startActivity(intent);
                finish();
            }
        });
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuyList.this, BuyFilters.class);
                intent.putExtra("subcat_id",subcat_id);
                intent.putExtra("catname",cat_name);
                intent.putExtra("sub_cat_name",sub_name);
                startActivity(intent);
                finish();
            }
        });
        if(sub_name.equals("All Varieties")){
            Namecategory=cat_name;
            Log.d("Namecategory:", "Namecategory" + Namecategory);

        }
        else{
            Namecategory=" ";
            Log.d("Namecategoryempty:", "Namecategoryempty" + Namecategory);
        }
        SellList();
        sell_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent=new Intent(BuyList.this,SingleCategory.class);
                intent.putExtra("hashme",addme);
                intent.putExtra("position",position);
                intent.putExtra("subcat_id",subcat_id);
                intent.putExtra("back", "buy");
                intent.putExtra("sub_cat_name", sub_name);
                intent.putExtra("catname", cat_name);

                startActivity(intent);
            }
        });
    }
    public void SellList(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url1="http://52.35.22.61/smart_crop/get_user_buy.php?sub_category_id="+subcat_id+"&user_id="+preferences.getString("uid","");

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("status");
                    if(status.equalsIgnoreCase("OK")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("users_buy");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject City = jsonArray.getJSONObject(i);
                            String id = City.getString("User_Contact_ID");
                            String Name = City.getString("Name");
                            String item = City.getString("item");
                            String Quantity = City.getString("Quantity");
                            String subcategory_name = City.getString("subcategory_name");
                            String Price = City.getString("Price");
                            String City_na = City.getString("city_name");
                            String Description = City.getString("Description");
                            String phone_number = City.getString("phone_number");
                            String chat_id = City.getString("chat_id");
                            String profile_image = City.getString("profile_file");
//                            String total = City.getString("total");
                            String min = City.getString("min");
                            String max = City.getString("max");

                            String categoryimage = City.getString("category_image");
                            String district=City.getString("district");
                            String Address=City.getString("Address");
                            Log.d("CatName", "CatName" + Name);
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("id", id);
                            hashMap.put("Name", Name);
                            hashMap.put("item", item);
                            hashMap.put("Quantity", Quantity);
                            hashMap.put("Price", Price);
                            hashMap.put("subcategory_name", subcategory_name);
                            hashMap.put("city_name", City_na);
                            hashMap.put("Description", Description);
                            hashMap.put("phone_number", phone_number);
                            hashMap.put("chat_id", chat_id);
                            hashMap.put("district", district);
                            hashMap.put("Address", Address);
                            hashMap.put("min", min);
                            hashMap.put("max", max);
                            hashMap.put("profile_file", profile_image);
                            hashMap.put("categoryimage", categoryimage);
//                            hashMap.put("total", total);


                            addme.add(hashMap);
                        }
                        sell_list.setAdapter(new SellListAdapter(BuyList.this, addme));
                    }else{
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(BuyList.this);
                        alertDialog.setTitle("              SmartCrop");
                        alertDialog.setMessage("Oops! no buyer in your area Be the first?");
//                        alertDialog.setIcon(R.drawable.add_icon);


                        alertDialog.setPositiveButton("YES",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Write your code here to execute after dialog
                                        Intent intent = new Intent(BuyList.this, Buy_post.class);
                                        intent.putExtra("subcat_id", subcat_id);
                                        intent.putExtra("catname", cat_name);
                                        intent.putExtra("sub_cat_name", sub_name);
                                        startActivity(intent);
                                        overridePendingTransition(0, 0);
                                        finish();
//                                        Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
                                        // tv.setText("Yes Button clicked");
                                    }
                                });
                        alertDialog.setNegativeButton("NO",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        //  Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                                        dialog.cancel();
                                        //  tv.setText("No Button clicked");
                                    }
                                });

                        alertDialog.show();
//                        new ShowMsg().createDialog(BuyList.this,"Oops! no buyer in your area Be the first?");

                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class SellListAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> addme;
        public SellListAdapter(Activity activity,ArrayList<HashMap<String,String>>addme){
            this.activity=activity;
            this.addme=addme;
        }
        @Override
        public int getCount() {
            return addme.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.selling_item,null);

            TextView name=(TextView)convertView.findViewById(R.id.name);
            TextView username=(TextView)convertView.findViewById(R.id.user_name);
            TextView kilo=(TextView)convertView.findViewById(R.id.kilo);
            TextView price=(TextView)convertView.findViewById(R.id.price);
            TextView phone=(TextView)convertView.findViewById(R.id.phone);
            ImageView chat=(ImageView)convertView.findViewById(R.id.chat);
            ImageView sell_image=(ImageView)convertView.findViewById(R.id.sell_image);
            TextView total_price=(TextView)convertView.findViewById(R.id.total_price);
            ImageView online=(ImageView)convertView.findViewById(R.id.online);
            TextView on=(TextView)convertView.findViewById(R.id.on);
total_price.setVisibility(View.GONE);
            chat.setTag(position);
            if(preferences.getString("uid","").equals(addme.get(position).get("id"))){
                chat.setVisibility(View.GONE);
            }else{
                chat.setVisibility(View.VISIBLE);
            }
            if(preferences.getString("Status","").equals("1")){
                chat.setBackgroundResource(R.drawable.chaton);
                online.setVisibility(View.VISIBLE);
                on.setVisibility(View.VISIBLE);
            }else{
                online.setVisibility(View.GONE);
                on.setVisibility(View.GONE);
                chat.setBackgroundResource(R.drawable.chatoff);
            }

            chat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id=(Integer)v.getTag();
                    Intent intent=new Intent(activity,ChatActivity.class);
                    intent.putExtra("chat_id",addme.get(id).get("chat_id"));
                    intent.putExtra("id",addme.get(id).get("id"));
                    intent.putExtra("name",addme.get(id).get("Name"));
//                    intent.putExtra("Price",addme.get(id).get("Price"));
//                    intent.putExtra("Price",addme.get(id).get("Price"));
                    activity.startActivity(intent);
                }
            });
            name.setText(addme.get(position).get("item"));
            username.setText(addme.get(position).get("Name"));
            kilo.setText(addme.get(position).get("Quantity"));
//            price.setText("RS. "+addme.get(position).get("Price"));
            phone.setText(addme.get(position).get("phone_number"));
            price.setText("RS: "+addme.get(position).get("min")+"-"+addme.get(position).get("max"));
            imageLoader.displayImage(addme.get(position).get("profile_file"), sell_image, options);
//

            return convertView;
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(BuyList.this,HomeCategory.class);

        startActivity(intent);
        finish();
    }
}
