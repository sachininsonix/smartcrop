package com.smartcrop;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by insonix on 17/11/15.
 */
public class SingleCategory extends Activity {
    TextView name,weight,price,time_city,desc,username,phone_num,on_status,descrip,smartcrop,sub_catname;
    ArrayList<HashMap<String,String>>hashme;
    ImageView chat,itemimage;
    int position;
    ProgressDialog progressDialog;
    String subcat_id,message=" ",uid,url,msel=" ",sub_category_id,cat,quantity,price1,state,phone,descrition,uname,userchatid,sub_cat_name,catname,max,min,item_image;
    ArrayList<HashMap<String,String >> addme;
    SharedPreferences preferences;
    ImageView back,setting;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_category);

        progressDialog=new ProgressDialog(SingleCategory.this);
        imageLoader=ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(SingleCategory.this));
        options=new DisplayImageOptions.Builder().showStubImage(R.drawable.images).showImageForEmptyUri(R.drawable.profile_image).cacheOnDisc().cacheInMemory().build();

        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        chat=(ImageView)findViewById(R.id.chat);
        itemimage=(ImageView)findViewById(R.id.item_image);
        name=(TextView)findViewById(R.id.name);
        descrip=(TextView)findViewById(R.id.descrip);
        sub_catname=(TextView)findViewById(R.id.sub_catname);
        weight=(TextView)findViewById(R.id.weight);
        smartcrop=(TextView)findViewById(R.id.smartcrop);
        price=(TextView)findViewById(R.id.price);
        time_city=(TextView)findViewById(R.id.time_city);
        desc=(TextView)findViewById(R.id.desc);
        username=(TextView)findViewById(R.id.username);
        phone_num=(TextView)findViewById(R.id.phone_num);
        on_status=(TextView)findViewById(R.id.on_status);
        hashme=new ArrayList<>();
        position=getIntent().getExtras().getInt("position");
       try
       {
           subcat_id=getIntent().getExtras().getString("subcat_id");
//        Log.d("subcat_id","subcat_id"+msel);
           sub_cat_name = getIntent().getExtras().getString("sub_cat_name").trim();
           catname = getIntent().getExtras().getString("catname").trim();
           Log.d("sub_name:", "sub_name" + sub_cat_name);
       }
       catch (Exception e){

       }

        try {
            message = getIntent().getExtras().getString("msg").trim();
        }
        catch(Exception e){

        }
        Intent intent = getIntent();


        if(!message.equals("A new post is added to sell in your wishlist")) {
          try {
              msel = getIntent().getExtras().getString("back");
              Log.d("msel","msel"+msel);
          }catch (Exception e){

          }
            hashme = (ArrayList<HashMap<String, String>>) intent.getSerializableExtra("hashme");
            if (preferences.getString("uid", "").equals(hashme.get(position).get("id"))) {
                chat.setVisibility(View.GONE);
            } else {
                chat.setVisibility(View.VISIBLE);
            }
            if(hashme.get(position).get("profile_file").equals("")){
                imageLoader.displayImage(hashme.get(position).get("categoryimage"), itemimage, options);

            }else{
                imageLoader.displayImage(hashme.get(position).get("profile_file"), itemimage, options);

            }
            name.setText(hashme.get(position).get("item"));
            if (hashme.get(position).get("Name").equals("")) {
                username.setText("No Name");
            } else {
                username.setText(hashme.get(position).get("Name"));
            }
            try{
                if (hashme.get(position).get("status").equals("1")) {
                    chat.setBackgroundResource(R.drawable.chaton);
                    on_status.setText("Online");
                } else {
                    chat.setBackgroundResource(R.drawable.chatoff);
                    on_status.setVisibility(View.GONE);
                }
            }
                catch(Exception e) {
                }

            weight.setText(hashme.get(position).get("Quantity"));
            sub_catname.setText(hashme.get(position).get("subcategory_name"));
            if(hashme.get(position).get("Price").equals("")){
                price.setText("RS. " + hashme.get(position).get("min") + "-" + hashme.get(position).get("max"));
            }else{
                price.setText("RS. " + hashme.get(position).get("Price"));
            }
            phone_num.setText(hashme.get(position).get("phone_number"));
            Log.d("msel", "msel" + msel);
            if(hashme.get(position).get("Description").equals("")){
             descrip.setVisibility(View.GONE);
            }else{
                descrip.setVisibility(View.VISIBLE);
                desc.setText(hashme.get(position).get("Description"));
            }
            if(hashme.get(position).get("Address").equals("")){
                time_city.setText(hashme.get(position).get("Address")+""+hashme.get(position).get("city_name"));
            }else {
                time_city.setText(hashme.get(position).get("Address") + "," + hashme.get(position).get("city_name"));
            }
            back = (ImageView) findViewById(R.id.menu);
            setting = (ImageView) findViewById(R.id.setting);
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (msel.equals("mysell")) {
                        Intent intent = new Intent(SingleCategory.this, Mysell.class);
                        intent.putExtra("subcat_id", subcat_id);
                        startActivity(intent);
                        finish();
                    }
                   else if (msel.equals("mybuy")) {
                        Intent intent = new Intent(SingleCategory.this, Mybuy.class);
                        intent.putExtra("subcat_id", subcat_id);
                        startActivity(intent);
                        finish();
                    } else if(msel.equals("sell")){
                        Intent intent = new Intent(SingleCategory.this, SellingList.class);
                        intent.putExtra("subcat_id", subcat_id);
                        intent.putExtra("sub_cat_name", sub_cat_name);
                        intent.putExtra("catname", catname);
                        startActivity(intent);
                        finish();
                } else if(msel.equals("buy")){
                    Intent intent = new Intent(SingleCategory.this, BuyList.class);
                    intent.putExtra("subcat_id", subcat_id);
                    intent.putExtra("sub_cat_name", sub_cat_name);
                    intent.putExtra("catname", catname);
                    startActivity(intent);
                    finish();
                }
                    else {
                        Intent intent = new Intent(SingleCategory.this, BuyList.class);
                        intent.putExtra("subcat_id", subcat_id);
                        startActivity(intent);
                        finish();
                    }
                }
            });
            setting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SingleCategory.this, SettingScrren.class);
                    startActivity(intent);
//                    finish();
                }
            });
            smartcrop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SingleCategory.this, HomeCategory.class);
                    startActivity(intent);
                    finish();
                }
            });
            chat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SingleCategory.this, ChatActivity.class);
                    intent.putExtra("chat_id", hashme.get(position).get("chat_id"));
                    intent.putExtra("name", hashme.get(position).get("Name"));
                    intent.putExtra("id", hashme.get(position).get("id"));
                    startActivity(intent);
                }
            });
            phone_num.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + phone_num.getText().toString()));//change the number
                    startActivity(callIntent);
                }
            });
        }
        else{

            message=getIntent().getExtras().getString("msg").trim();
            uid=getIntent().getExtras().getString("uid").trim();
            userchatid=getIntent().getExtras().getString("chatid").trim();
            sub_category_id=getIntent().getExtras().getString("sub_category_id").trim();
            cat=getIntent().getExtras().getString("cat").trim();
            quantity=getIntent().getExtras().getString("quantity").trim();
            max=getIntent().getExtras().getString("max").trim();
            min=getIntent().getExtras().getString("min").trim();
            state=getIntent().getExtras().getString("state").trim();
            phone=getIntent().getExtras().getString("phone").trim();
            descrition=getIntent().getExtras().getString("description").trim();
            uname=getIntent().getExtras().getString("name").trim();
            item_image=getIntent().getExtras().getString("category_image").trim();

            chat.setVisibility(View.VISIBLE);
//            }
            name.setText(cat);
            if (uname.equals("")) {
                username.setText("No Name");
            } else {
                username.setText(uname);
            }
            if (preferences.getString("Status", "").equals("1")) {
                chat.setBackgroundResource(R.drawable.chaton);
                on_status.setText("Online");
            } else {
                chat.setBackgroundResource(R.drawable.chatoff);
                on_status.setVisibility(View.GONE);
            }
            weight.setText(quantity);
            price.setText("RS. " + min + "-" + max);
            phone_num.setText(phone);
            if(descrition.equals("")){
                descrip.setVisibility(View.GONE);
            }else{
                descrip.setVisibility(View.VISIBLE);
                desc.setText(descrition);
            }
            time_city.setText(state);
            imageLoader.displayImage(item_image, itemimage, options);
            back = (ImageView) findViewById(R.id.menu);
            setting = (ImageView) findViewById(R.id.setting);
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (msel.equals("mysell")) {
                        Intent intent = new Intent(SingleCategory.this, Mysell.class);
                        intent.putExtra("subcat_id", sub_category_id);
                        startActivity(intent);
                        finish();
                    }
                    else if (msel.equals("mybuy")) {
                        Intent intent = new Intent(SingleCategory.this, Mybuy.class);
                        intent.putExtra("subcat_id", sub_category_id);
                        startActivity(intent);
                        finish();
                    } else if(msel.equals("sell")){
                        Intent intent = new Intent(SingleCategory.this, SellingList.class);
                        intent.putExtra("subcat_id", sub_category_id);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        Intent intent = new Intent(SingleCategory.this, BuyList.class);
                        intent.putExtra("subcat_id", sub_category_id);
                        startActivity(intent);
                        finish();
                    }

                }
            });
            setting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SingleCategory.this, SettingScrren.class);
                    startActivity(intent);
                    finish();
                }
            });
            chat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SingleCategory.this, ChatActivity.class);
                    intent.putExtra("chat_id", userchatid);
                    intent.putExtra("id", uid);
                    startActivity(intent);
                }
            });
            phone_num.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + phone_num.getText().toString()));//change the number
                    startActivity(callIntent);
                }
            });

        }
//

    }
}