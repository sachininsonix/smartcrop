package com.smartcrop;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by insonix on 14/12/15.
 */
public class Editprofile extends Activity {
        EditText name,address,mobileno;
    Button save;
    TextView smartcrop;
    ProgressDialog progressDialog;
    String url,url2,file;
    static int w=0, h=0;
    Dialog imageDialog;
    protected  static final int IMAGE_CAMERA_11=11;
    protected static final int IMAGE_GALLERY_22 = 22;
    Bitmap bitmap;
    ImageView back,setting,sell_image;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editprofile);
        Display display = getWindowManager().getDefaultDisplay();
        w = display.getWidth();
        h= display.getHeight();
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor=preferences.edit();
        progressDialog=new ProgressDialog(Editprofile.this);
        imageLoader=ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(Editprofile.this));
        options=new DisplayImageOptions.Builder().showStubImage(R.drawable.images).showImageForEmptyUri(R.drawable.profile_image).cacheOnDisc().cacheInMemory().build();
        sell_image=(ImageView)findViewById(R.id.sell_image);
        back=(ImageView)findViewById(R.id.menu);
        smartcrop=(TextView)findViewById(R.id.smartcrop);
        setting=(ImageView)findViewById(R.id.setting);
        name =(EditText)findViewById(R.id.name);
        address=(EditText)findViewById(R.id.address);
        mobileno=(EditText)findViewById(R.id.mobileno);
        save=(Button)findViewById(R.id.save);
        imageDialog = new Dialog(this);
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = imageDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        imageDialog.setContentView(R.layout.options);
        LinearLayout approx_lay = (LinearLayout) imageDialog.findViewById(R.id.approx_lay);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(w - 30, (h / 3) - 20);
        approx_lay.setLayoutParams(params);
        sell_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageDialog.show();
            }
        });
        mobileno.setText(preferences.getString("phone_number", ""));
        address.setText(preferences.getString("Address",""));
//        name.setText(preferences.getString("username",""));
        if(preferences.getString("photo_path","").equals("")){

        }else{
            imageLoader.displayImage(preferences.getString("photo_path",""),sell_image,options);

        }
        if(preferences.getString("username","").equals("")){
            name.setText("");
        }else{
            name.setText(preferences.getString("username",""));
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Editprofile.this, MyAccount.class);
                startActivity(intent);
                finish();
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Editprofile.this, SettingScrren.class);
                startActivity(intent);
//                finish();
            }
        });
        smartcrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Editprofile.this,HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });
        TextView Invite_mail = (TextView) imageDialog.findViewById(R.id.options_camera);
        Invite_mail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//					startActivityForResult(intent, IMAGE_CAMERA_11);

                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, IMAGE_CAMERA_11);
                imageDialog.dismiss();
            }
        });
        TextView Invite_sms = (TextView) imageDialog.findViewById(R.id.options_gallery);
        Invite_sms.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new  Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, IMAGE_GALLERY_22);
                imageDialog.dismiss();
            }
        });
        TextView Invite_cancel = (TextView) imageDialog.findViewById(R.id.options_cancel);
        Invite_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                imageDialog.dismiss();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mobNo = mobileno.getText().toString().trim();



                            if ((mobNo.length() >= 5) && (mobNo.length() <= 10)) {
                                EditProfileTask();

                            } else {
                                Toast.makeText(Editprofile.this, " Mobile Number must be less than 10 digits ", Toast.LENGTH_SHORT).show();
                                mobileno.requestFocus();
                            }
                        }


            });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case IMAGE_CAMERA_11:
                if(data!=null){
                    Uri uri=data.getData();
                    try{
                        bitmap=new UserPicture(uri,getContentResolver()).getBitmap();
                        sell_image.setImageBitmap(bitmap);
                    }catch(Exception e){

                    }
                }else{

                }
                break;
            case IMAGE_GALLERY_22:
                if(data!=null){
                    Uri uri=data.getData();
                    try{
                        bitmap=new UserPicture(uri,getContentResolver()).getBitmap();
                        sell_image.setImageBitmap(bitmap);
                    }catch(Exception e){

                    }
                }else{

                }
                break;
        }
    }
    public void EditProfileTask() {
        url2 = "http://52.35.22.61/smart_crop/edit_user_contact.php?";
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Log.d("url,...... ","url isssss"+url2);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url2, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                Log.d("sss", "sss" + s);
                try{
                    JSONObject object=new JSONObject(s);
                    String Status=object.getString("Status");
                    if(Status.equalsIgnoreCase("OK")){


                        editor.putString("username",name.getText().toString());
                        editor.commit();
                        Intent intent = new Intent(Editprofile.this, MyAccount.class);
                        startActivity(intent);
                        Toast.makeText(Editprofile.this, "Successfully updated", Toast.LENGTH_SHORT).show();
                        finish();






                    }else{

                    }
                }catch(Exception e){

                }
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
            }


        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                ByteArrayOutputStream arrayOutputStream=new ByteArrayOutputStream();

                if(bitmap==null){

                }else {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, arrayOutputStream);
                    byte[] data = arrayOutputStream.toByteArray();
                    file = Base64.encodeToString(data, Base64.DEFAULT);
                }
                params.put("id", preferences.getString("uid",""));
                params.put("name", name.getText().toString().trim());
                params.put("address", address.getText().toString().trim());
                params.put("phone_number",mobileno.getText().toString().trim());

                if(file==null){
//
                }else {

                    params.put("profile_name", "" + new Date().getTime() + ".png");
                    params.put("profile_file", file);
                }

                return (params);
            }
//            private Map<String, String> checkParams(Map<String, String> map){
//                Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
//                while (it.hasNext()) {
//                    Map.Entry<String, String> pairs = (Map.Entry<String, String>)it.next();
//                    if(pairs.getValue()==null){
//                        map.put(pairs.getKey(), "");
//                    }
//                }
//                return map;
//            }
//
//
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Editprofile.this, MyAccount.class);
        startActivity(intent);
        finish();
    }
}


