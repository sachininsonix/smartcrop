package com.smartcrop;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by insonix on 17/11/15.
 */
public class AllCategory extends Activity {
    ListView category;
    TextView smartcrop;
    LinearLayout citylist;
    Typeface font;
    ProgressDialog progressDialog;
    ArrayList<HashMap<String,String >>addme;
    String url,city_id,lang;
    EditText search_category;
    CategoryAdapter categoryAdapter;
    ArrayList<HashMap<String,String >>searchlist;
    ImageView back,setting,no_found;
    SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_categoey_list);
        progressDialog=new ProgressDialog(AllCategory.this);
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        addme=new ArrayList<>();
//        font = Typeface.createFromAsset(getAssets(), "bulara_5.ttf");
        category=(ListView)findViewById(R.id.category);
        smartcrop=(TextView)findViewById(R.id.smartcrop);
        search_category=(EditText)findViewById(R.id.search_category);
        citylist=(LinearLayout)findViewById(R.id.citylist);
        try {
            city_id = getIntent().getExtras().getString("city_id");
        }catch (Exception e) {

        }
        if(preferences.getString("on","").equals("1")){
            lang="1";
//
        }else if(preferences.getString("on","").equals("2")){
            lang="2";
//
        }else{
            lang="0";
        }
        Log.d("city_id","city_id"+city_id);
        back=(ImageView)findViewById(R.id.menu);
        no_found=(ImageView)findViewById(R.id.no_found);
        setting=(ImageView)findViewById(R.id.setting);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(AllCategory.this,HomeCategory.class);
//                startActivity(intent);
//                finish();
//                if(category.getVisibility()==View.VISIBLE ){
//                    category.setVisibility(View.GONE);
//                }
//                else{
                    Intent intent = new Intent(AllCategory.this, HomeCategory.class);
                    startActivity(intent);
                    finish();
//                }
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(AllCategory.this,SettingScrren.class);
                startActivity(intent);
//                finish();
            }
        });
        smartcrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AllCategory.this,HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });
        try {
            AllCategoryList();
        }catch (Exception e){

        }
        search_category.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String searchString = search_category.getText().toString();
                int textLength = searchString.length();
                searchlist.clear();
                for (int i = 0; i < addme.size(); i++) {
                    String catename = addme.get(i).get("catname").toString();
                    if (textLength <= catename.length()) {
                        if (searchString.equalsIgnoreCase(catename.substring(0, textLength)))
                            searchlist.add(addme.get(i));

                    }
                }
//                sub_category.setAdapter(subCategoryAdapter);
                categoryAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void AllCategoryList(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url="http://52.35.22.61/smart_crop/get_all_categories.php?city_id="+URLEncoder.encode(city_id)+"&lang="+lang;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("Status");
                    if(status.equalsIgnoreCase("OK")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("category");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject catlist = jsonArray.getJSONObject(i);
                            String id = catlist.getString("ID");
                            String CatName = catlist.getString("CatName");

                            Log.d("CatName", "CatName" + CatName);
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("catname", CatName);

                            hashMap.put("catid", id);
                            addme.add(hashMap);
                        }
                        searchlist = new ArrayList<>(addme);
                        categoryAdapter = new CategoryAdapter(AllCategory.this, searchlist);
                        category.setAdapter(categoryAdapter);
                        no_found.setVisibility(View.GONE);
                    }else{
                        new ShowMsg().createDialog(AllCategory.this,"No record found");
                        no_found.setVisibility(View.VISIBLE);
                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class CategoryAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> addme;
        public CategoryAdapter(Activity activity,ArrayList<HashMap<String,String>>addme){
            this.activity=activity;
            this.addme=addme;
        }
        @Override
        public int getCount() {
            return addme.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.all_category_item,null);
//            ImageView cat_image=(ImageView)convertView.findViewById(R.id.cat_image);
            TextView cat_name=(TextView)convertView.findViewById(R.id.cat_name);
            RelativeLayout rel_help=(RelativeLayout)convertView.findViewById(R.id.rel_help);
            cat_name.setText(addme.get(position).get("catname"));
//            cat_name.setTypeface(font);
            rel_help.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, SubCategory.class);
                    intent.putExtra("catname", addme.get(position).get("catname"));
                    intent.putExtra("catid", addme.get(position).get("catid"));
                    intent.putExtra("city_id",city_id);
                    activity.startActivity(intent);
                }
            });


            return convertView;
        }
    }

    @Override
    public void onBackPressed() {

//        if (category.getVisibility() == View.VISIBLE) {
//            category.setVisibility(View.GONE);
//        } else {
            Intent intent = new Intent(AllCategory.this, HomeCategory.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
//        }
    }
}
