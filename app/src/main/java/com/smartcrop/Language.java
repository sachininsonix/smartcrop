package com.smartcrop;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.Locale;

/**
 * Created by insonix on 2/12/15.
 */
public class Language extends Activity {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    ToggleButton toggleButton,toggleButton2,toggleButton3;
    ImageView back,setting;
    TextView title,textView2,textView1,textView3,set;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.language);
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor=preferences.edit();
        textView1=(TextView)findViewById(R.id.textView1);
        toggleButton3=(ToggleButton)findViewById(R.id.toggleButton23);
        textView3=(TextView)findViewById(R.id.textView3);
        textView2=(TextView)findViewById(R.id.textView2);
        title=(TextView)findViewById(R.id.title);
        set=(TextView)findViewById(R.id.set);
        toggleButton=(ToggleButton)findViewById(R.id.toggleButton11);

        toggleButton2=(ToggleButton)findViewById(R.id.toggleButton21);
        back=(ImageView)findViewById(R.id.menu);
        setting=(ImageView)findViewById(R.id.setting);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Language.this,SettingScrren.class);
                startActivity(intent);
                finish();
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Language.this,SettingScrren.class);
                startActivity(intent);
//                finish();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Language.this, HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });

        if(preferences.getString("on","").equals("1")){
            String languageToLoad  = "pa";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            textView1.setText("English");
            title.setText("ਸਮਾਰਟ ਫਸਲ");
            textView2.setText("ਪੰਜਾਬੀ");
            textView3.setText("हिंदी");
            title.setText("ਸਮਾਰਟ ਫਸਲ");
            toggleButton2.setChecked(true);
            set.setText("ਭਾਸ਼ਾ ਚੁਣੋ");
            toggleButton.setChecked(false);
            toggleButton3.setChecked(false);
        }else if (preferences.getString("on","").equals("2")){
            String languageToLoad = "hi";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            title.setText("स्मार्ट को्प  ");
            toggleButton2.setChecked(false);
            toggleButton.setChecked(false);
            toggleButton3.setChecked(true);
            textView1.setText("English");
            set.setText("भाषा चुनें");

            textView2.setText("ਪੰਜਾਬੀ");
            textView3.setText("हिंदी");
        }else{
            String languageToLoad = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            toggleButton2.setChecked(false);
            toggleButton.setChecked(true);
            toggleButton3.setChecked(false);
            textView1.setText("English");
            title.setText("SMART CROP");
            set.setText("Choose language");
            textView2.setText("ਪੰਜਾਬੀ");
            textView3.setText("हिंदी");
        }
        toggleButton2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked==true){
                    editor.putString("on","1");
                    editor.commit();
                    String languageToLoad  = "pa";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    toggleButton.setChecked(false);
                    toggleButton3.setChecked(false);
                    textView1.setText("English");
                    title.setText("ਸਮਾਰਟ ਫਸਲ");
                    set.setText("ਭਾਸ਼ਾ ਚੁਣੋ");

                    textView2.setText("ਪੰਜਾਬੀ");
                    textView3.setText("हिंदी");
                }else if(toggleButton.isChecked()){
                    editor.putString("on", "0");
                    editor.commit();

                    toggleButton.setChecked(true);
                    toggleButton3.setChecked(false);
                    toggleButton2.setChecked(false);
                    textView1.setText("English");
                    title.setText("SMART CROP");
                    textView2.setText("ਪੰਜਾਬੀ");
                    textView3.setText("हिंदी");
                    set.setText("Choose language");
                }else{
                    editor.putString("on", "2");
                    editor.commit();
                    toggleButton2.setChecked(false);
                    toggleButton.setChecked(false);
                    toggleButton3.setChecked(true);
                    textView1.setText("English");
                    title.setText("स्मार्ट को्प  ");
                    textView2.setText("ਪੰਜਾਬੀ");
                    textView3.setText("हिंदी");
                    set.setText("भाषा चुनें");
                }
            }
        });
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true) {
                    editor.putString("on", "0");
                    editor.commit();
                    String languageToLoad = "en";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    toggleButton2.setChecked(false);
                    toggleButton3.setChecked(false);
                    title.setText("SMART CROP");
                    textView1.setText("English");
                    textView2.setText("ਪੰਜਾਬੀ");
                    textView3.setText("हिंदी");
                    set.setText("Choose language");
                } else if (toggleButton2.isChecked()) {
                    editor.putString("on", "1");
                    editor.commit();
                    toggleButton.setChecked(false);
                    toggleButton3.setChecked(false);
                    toggleButton2.setChecked(true);
                    textView1.setText("English");
                    textView2.setText("ਪੰਜਾਬੀ");
                    textView3.setText("हिंदी");
                    title.setText("ਸਮਾਰਟ ਫਸਲ");
                    set.setText("ਭਾਸ਼ਾ ਚੁਣੋ");
                } else {
                    editor.putString("on", "2");
                    editor.commit();
                    toggleButton2.setChecked(false);
                    toggleButton.setChecked(false);
                    toggleButton3.setChecked(true);
                    textView1.setText("English");
                    title.setText("स्मार्ट को्प  ");
                    textView2.setText("ਪੰਜਾਬੀ");
                    textView3.setText("हिंदी");
                    set.setText("भाषा चुनें");
                }
            }
        });
        toggleButton3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true) {
                    editor.putString("on", "2");
                    editor.commit();
                    String languageToLoad = "hi";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    toggleButton2.setChecked(false);
                    toggleButton.setChecked(false);
                    title.setText("स्मार्ट को्प  ");
                    textView1.setText("English");
                    textView2.setText("ਪੰਜਾਬੀ");
                    textView3.setText("हिंदी");
                    set.setText("भाषा चुनें");
                } else if (toggleButton2.isChecked()) {
                    editor.putString("on", "1");
                    editor.commit();
                    toggleButton.setChecked(false);
                    toggleButton3.setChecked(false);
                    toggleButton2.setChecked(true);
                    textView1.setText("English");
                    textView2.setText("ਪੰਜਾਬੀ");
                    textView3.setText("हिंदी");
                    title.setText("ਸਮਾਰਟ ਫਸਲ");
                    set.setText("ਭਾਸ਼ਾ ਚੁਣੋ");
                } else {
                    editor.putString("on", "0");
                    editor.commit();
                    toggleButton2.setChecked(false);
                    toggleButton.setChecked(true);
                    toggleButton3.setChecked(false);
                    textView1.setText("English");
                    title.setText("SMART CROP");
                    textView2.setText("ਪੰਜਾਬੀ");
                    textView3.setText("हिंदी");
                    set.setText("Choose language");
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(Language.this,SettingScrren.class);
        startActivity(intent);
        finish();
    }
}
