package com.smartcrop;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Locale;

/**
 * Created by insonix on 10/11/15.
 */
public class SlashActivity extends Activity {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String lang;
    ImageView splash_logo;
    Thread splashTread;
    final Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor=preferences.edit();
        splash_logo=(ImageView)findViewById(R.id.splash_logo);
        StartAnimations();

    }
    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        RelativeLayout l=(RelativeLayout) findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(anim);
        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        ImageView iv = (ImageView) findViewById(R.id.splash_logo);
        iv.clearAnimation();
        iv.startAnimation(anim);
        if(preferences.getString("on","").equals("1")){

            String languageToLoad  = "pa";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }else{

            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }
        if(Network.isNetworkAvailable(SlashActivity.this)) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(4000);

                    if(preferences.getString("uid","").equals("")) {
                        Intent intent = new Intent(SlashActivity.this, PagerView.class);
                        startActivity(intent);
                        finish();
                    }else{
                        Intent intent = new Intent(SlashActivity.this, HomeCategory.class);
                        startActivity(intent);
                        finish();
                    }
                }catch (Exception e){

                }

            }
        }).start();
        }else{

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    new ShowMsg().createDialog(SlashActivity.this, "No Network Available! To proceed further please enable Internet.");

                }
            }, 3000);


        }
    }
}
