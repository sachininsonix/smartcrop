package com.smartcrop;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Message;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by insonix on 17/11/15.
 */
public class HomeCategory extends Activity implements View.OnClickListener {
    GridView category_list;
    String url,url1,city_id,url2,url3,lang,cityid;
    ListView city_list;
    GPSTracker gpsTracker;
    LinearLayout show_more_lin,lin_user,lin_chat,citylist,lin_mandy;
SharedPreferences preferences;
    SharedPreferences.Editor editor ;
    Typeface font;
    ImageView no_image;
    RelativeLayout mainn;
    ProgressDialog progressDialog;
    ArrayList<HashMap<String,String >>addme;
    ArrayList<HashMap<String,String >>city_listing;
    ArrayList<HashMap<String,String >>search_listing;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    EditText city_name_c;
    ImageView back,setting;
    int id;
    TextView showmore,pop,man,mych,myacc;
    LocationAddress locationAddress;
    Button apply;
    InputMethodManager imm;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.home_category);
        progressDialog=new ProgressDialog(HomeCategory.this);
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor=preferences.edit();
        addme=new ArrayList<>();
        gpsTracker=new GPSTracker(HomeCategory.this);
        font = Typeface.createFromAsset(getAssets(), "bulara_5.ttf");
        imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
showmore=(TextView)findViewById(R.id.show_more);
        man=(TextView)findViewById(R.id.man);
        mych=(TextView)findViewById(R.id.mych);
        myacc=(TextView)findViewById(R.id.myacc);
        pop=(TextView)findViewById(R.id.pop);

        city_listing=new ArrayList<>();
        imageLoader=ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(HomeCategory.this));
        options=new DisplayImageOptions.Builder().showStubImage(R.drawable.images).showImageForEmptyUri(R.drawable.profile_image).cacheOnDisc().cacheInMemory().build();
        category_list=(GridView)findViewById(R.id.category_list);
        no_image=(ImageView)findViewById(R.id.no_image);
        city_list=(ListView)findViewById(R.id.city_list);

        lin_mandy=(LinearLayout)findViewById(R.id.lin_mandy);
        lin_mandy.setOnClickListener(this);
        mainn=(RelativeLayout)findViewById(R.id.mainn);
//        mainlin1=(ImageView)findViewById(R.id.mainlin1);
        apply=(Button)findViewById(R.id.apply);
        apply.setOnClickListener(this);
        city_name_c=(EditText)findViewById(R.id.city_name_c);

        city_name_c.setOnClickListener(this);
        show_more_lin=(LinearLayout)findViewById(R.id.show_more_lin);
        show_more_lin.setOnClickListener(this);
//
        lin_user=(LinearLayout)findViewById(R.id.lin_user);
        lin_user.setOnClickListener(this);
        lin_chat=(LinearLayout)findViewById(R.id.lin_chat);
        lin_chat.setOnClickListener(this);
        citylist=(LinearLayout)findViewById(R.id.citylist);
        citylist.setOnClickListener(this);
        back=(ImageView)findViewById(R.id.menu);
        setting=(ImageView)findViewById(R.id.setting);
//        Location location=gpsTracker.getLocation();
        double latitude = gpsTracker.getLatitude();
        double longitude = gpsTracker.getLongitude();
        Log.d("latitude:","latitude"+latitude);
        Log.d("longitude:","longitude"+longitude);
        if (gpsTracker.canGetLocation()) {
//            double latitude = gpsTracker.getLatitude();
//            double longitude = gpsTracker.getLongitude();
//            String languageToLoad = "en";
//            Locale locale = new Locale(languageToLoad);
//            Locale.setDefault(locale);
//            Configuration config = new Configuration();
//            config.locale = locale;
//            getBaseContext().getResources().updateConfiguration(config,
//                    getBaseContext().getResources().getDisplayMetrics());
             locationAddress = new LocationAddress();
            locationAddress.getAddressFromLocation(latitude, longitude,
                    getApplicationContext(), new GeocoderHandler());
            Log.d("locationAddress:", "locationAddress" + locationAddress);
        } else {
            gpsTracker.showSettingsAlert();
        }
        if(preferences.getString("on","").equals("1")){
            String languageToLoad  = "pa";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            lang="1";
            apply.setText("ਅਪਲਾੲੀ ");
            showmore.setText("ਸ਼ੌ ਮੋਰ ਕੈਟਿਗੀਰੀ");
            pop.setText( "ਪਾਪੁਲਰ ਕੈਟੇਗਰੀਯਾਂ");


            man.setText("ਮੰਡੀ ਰੇਟ");
            mych.setText("ਮੇਰੀ ਚੈਟ");
            myacc.setText("ਮੇਰਾ ਖਾਤਾ");
//
        }else if(preferences.getString("on","").equals("2")){
            String languageToLoad  = "hi";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            lang="2";
            apply.setText("एप्लाई");
            showmore.setText("और केटेगरी  दिखाएं");
            pop.setText("पोपुलर केटेग़री");
            man.setText("मंडी रेट");
            mych.setText("मेरे चैट");
            myacc.setText("माई अकाउंट");

//
        }else{
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            lang="0";
        }
        mainn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                city_list.setVisibility(View.GONE);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(HomeCategory.this,HomeCategory.class);
//                startActivity(intent);
                finish();
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (city_name_c.getText().toString().length() > 0) {
                    Intent intent = new Intent(HomeCategory.this, SettingScrren.class);
                    startActivity(intent);
                } else {
                    new ShowMsg().createDialog(HomeCategory.this, "Please select State");
                }
//                finish();
            }
        });
//

        CityList();
//        CategoryList();

//
//        city_id = preferences.getString("cid","");
        city_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                city_list.setVisibility(View.GONE);
//
                imm.hideSoftInputFromWindow(city_name_c.getWindowToken(), 0);

//                editor.putString("cname", city_name_c.getText().toString().trim());
////                editor.putString("cname",city_listing.get(position).get("City_Name"));
//
//                editor.commit();
                city_name_c.setText(city_listing.get(position).get("City_Name"));
//
//                city_id = preferences.getString("cid","");
                Log.d("ccc", "ccc" + city_id);
//                url = "http://demo.insonix.com/smart_crop/get_main_categories.php?city_id="+city_id+"&lang=0";

            }
        });
        city_name_c.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchCityList();
            }

            @Override
            public void afterTextChanged(Editable s) {
//searchString=city_name_c.getText().toString();

            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.show_more_lin:
                if(city_name_c.getText().toString().length()>0) {
                    Intent intent=new Intent(HomeCategory.this,AllCategory.class);
                    intent.putExtra("city_id", city_name_c.getText().toString());
                    startActivity(intent);
                }else{
                    new ShowMsg().createDialog(HomeCategory.this,"Please select State");
                }

                break;
            case R.id.lin_chat:
                Intent chat=new Intent(HomeCategory.this,ChatHistory.class);
                startActivity(chat);
                break;
            case R.id.lin_user:
                Intent user=new Intent(HomeCategory.this,MyAccount.class);
                startActivity(user);
                break;
            case R.id.lin_mandy:
                Intent mand=new Intent(HomeCategory.this,MandiRate.class);
                startActivity(mand);
                break;
            case R.id.citylist:
                city_name_c.setText("");
                city_list.setVisibility(View.VISIBLE);
//                no_image.setVisibility(View.GONE);
//

                city_list.setAdapter(new CityAdapter(HomeCategory.this, city_listing));

                break;
            case R.id.city_name_c:
                city_name_c.setText("");
                city_list.setVisibility(View.VISIBLE);
//                no_image.setVisibility(View.GONE);
//

                city_list.setAdapter(new CityAdapter(HomeCategory.this, city_listing));

                break;
            case R.id.apply:
   if(city_name_c.getText().toString().length()>0) {
       editor.putString("cname", city_name_c.getText().toString().trim());
       editor.commit();
       imm.hideSoftInputFromWindow(city_name_c.getWindowToken(), 0);
       city_list.setVisibility(View.GONE);
       CategoryList();
   }else{
       new ShowMsg().createDialog(HomeCategory.this,"Please select State");
   }

                break;

        }
    }

    public void CategoryList() {
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url = "http://52.35.22.61/smart_crop/get_main_categories.php?city_id="+ URLEncoder.encode(preferences.getString("cname",""))+"&lang="+lang;
        Log.d("urlurlurl:", "urlurl" + url);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                addme.clear();
                try{
                    String status=jsonObject.getString("Status");
                    if(status.equalsIgnoreCase("OK")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("category")
;
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject catlist = jsonArray.getJSONObject(i);
                            String id = catlist.getString("ID");
                            String CatName = catlist.getString("CatName");
                            String CatImage = catlist.getString("Cat_thumb_image");
                            Log.d("CatName", "CatName" + CatName);
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("catname", CatName);
                            hashMap.put("catimage", CatImage);
                            hashMap.put("catid", id);
                            addme.add(hashMap);
                        }
                        category_list.setVisibility(View.VISIBLE);
                        no_image.setVisibility(View.GONE);
                        city_list.setVisibility(View.GONE);
                        category_list.setAdapter(new CategoryAdapter(HomeCategory.this, addme));
                    }else{
                        category_list.setVisibility(View.INVISIBLE);
                        no_image.setVisibility(View.VISIBLE);
                        city_list.setVisibility(View.GONE);
//                        no_image.setBackgroundResource(R.drawable.no_img);
                        new ShowMsg().createDialog(HomeCategory.this, "No record found");
                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class CategoryAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>>addme;
        public CategoryAdapter(Activity activity,ArrayList<HashMap<String,String>>addme){
            this.activity=activity;
            this.addme=addme;
        }
        @Override
        public int getCount() {
            return addme.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.home_list,null);
            ImageView cat_image=(ImageView)convertView.findViewById(R.id.cat_image);
            TextView cat_name=(TextView)convertView.findViewById(R.id.cat_name);
            cat_name.setText(addme.get(position).get("catname"));
            imageLoader.displayImage(addme.get(position).get("catimage"), cat_image, options);
            cat_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(activity,SubCategory.class);
                    intent.putExtra("catid",addme.get(position).get("catid"));
                    intent.putExtra("catname",addme.get(position).get("catname"));

                    intent.putExtra("city_id", city_name_c.getText().toString());
                    activity.startActivity(intent);
                    finish();
                }
            });
            return convertView;
        }
    }
//
    public void CityList(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url1="http://52.35.22.61/smart_crop/get_all_cities.php?search="+city_name_c.getText().toString();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());

                try{
                    String status=jsonObject.getString("Status");

                    JSONArray jsonArray=jsonObject.getJSONArray("City");
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject City=jsonArray.getJSONObject(i);
                        String id=City.getString("ID");
                        String City_Name = City.getString("City_Name");

                        Log.d("CatName", "CatName" + City_Name);
                        HashMap<String,String> hashMap=new HashMap<>();
                        hashMap.put("City_Name", City_Name);
                        hashMap.put("City_id", id);

                        city_listing.add(hashMap);

                    }
                    city_list.setAdapter(new CityAdapter(HomeCategory.this, city_listing));
//                    city_name_c.setText(city_listing.get(0).get("City_Name"));
//                    cityid = city_listing.get(0).get("City_id");
                    CategoryList();


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    public void searchCityList(){
//
        url3="http://52.35.22.61/smart_crop/get_all_cities.php?search="+city_name_c.getText().toString();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url3, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
city_listing.clear();
                try{
                    String status=jsonObject.getString("Status");

                    JSONArray jsonArray=jsonObject.getJSONArray("City");
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject City=jsonArray.getJSONObject(i);
                        String id=City.getString("ID");
                        String City_Name = City.getString("City_Name");

                        Log.d("CatName", "CatName" + City_Name);
                        HashMap<String,String> hashMap=new HashMap<>();
                        hashMap.put("City_Name", City_Name);
                        hashMap.put("City_id", id);

                        city_listing.add(hashMap);

                    }
                    city_list.setAdapter(new CityAdapter(HomeCategory.this, city_listing));
//                    city_name_c.setText(city_listing.get(0).get("City_Name"));
//                    city_id = city_listing.get(0).get("City_id");
//                    CategoryList();


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class CityAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> city_listing;
        public CityAdapter(Activity activity,ArrayList<HashMap<String,String>>city_listing){
            this.activity=activity;
            this.city_listing=city_listing;
        }
        @Override
        public int getCount() {
            return city_listing.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.city_item,null);

            final TextView city_name=(TextView)convertView.findViewById(R.id.city_name);
//            RelativeLayout rel_help=(RelativeLayout)convertView.findViewById(R.id.rel_help);
//            rel_help.setTag(position);

            city_name.setText(city_listing.get(position).get("City_Name"));



            return convertView;
        }
    }
    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
//                    String languageToLoad = "en";
//                Locale locale = new Locale(languageToLoad);
//                Locale.setDefault(locale);
//                Configuration config = new Configuration();
//                config.locale = locale;
//                getBaseContext().getResources().updateConfiguration(config,
//                        getBaseContext().getResources().getDisplayMetrics());
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            if(preferences.getString("cname","").equals("")){
//                String languageToLoad = "en";
//                Locale locale = new Locale(languageToLoad);
//                Locale.setDefault(locale);
//                Configuration config = new Configuration();
//                config.locale = locale;
//                getBaseContext().getResources().updateConfiguration(config,
//                        getBaseContext().getResources().getDisplayMetrics());
                editor.putString("cname", locationAddress);
                editor.commit();
                city_name_c.setText(locationAddress);
            }else {
                city_name_c.setText(preferences.getString("cname", ""));
            }
//            Toast.makeText(HomeCategory.this,locationAddress,Toast.LENGTH_LONG).show();
            Log.d("locationAddress","locationAddress"+locationAddress);
//            tvAddress.setText(locationAddress);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(city_list.getVisibility()==View.VISIBLE){
            city_list.setVisibility(View.GONE);
        }else{
//            Intent startMain = new Intent(Intent.ACTION_MAIN);
//            startMain.addCategory(Intent.CATEGORY_HOME);
//            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(startMain);
//            int pid = android.os.Process.myPid();
//            android.os.Process.killProcess(pid);
            finish();
        }


    }
}
