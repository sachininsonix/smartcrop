package com.smartcrop;

/**
 * @author greg
 * @since 6/21/13
 */
public class Chat {

    private String message;
    private String chatID;
    private String time;
    private String image;
    private String type;


    // Required default constructor for Firebase object mapping
    @SuppressWarnings("unused")
    private Chat() {
    }

    Chat(String message, String chatID,String time,String type) {
        this.message = message;
        this.chatID = chatID;
        this.time = time;
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public String getImage() {
        return image;
    }

    public String getMessage() {
        return message;
    }

    public String getChatID() {
        return chatID;
    }

    
   
    public String getType() {
		return type;
	}
}
