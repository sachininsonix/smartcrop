package com.smartcrop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by insonix on 17/11/15.
 */
public class SettingScrren extends Activity implements View.OnClickListener {
    RelativeLayout rel_help,rel_num,rel_wish,rel_noti,rel_account,rel_lang,rel_feed;
    ImageView back;
    TextView smartcrop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_screen);
        rel_help=(RelativeLayout)findViewById(R.id.rel_help);
        rel_help.setOnClickListener(this);
        rel_noti=(RelativeLayout)findViewById(R.id.rel_noti);
        rel_noti.setOnClickListener(this);
        rel_account=(RelativeLayout)findViewById(R.id.rel_account);
        rel_account.setOnClickListener(this);
        rel_wish=(RelativeLayout)findViewById(R.id.rel_wish);
        rel_wish.setOnClickListener(this);
        rel_lang=(RelativeLayout)findViewById(R.id.rel_lang);
        rel_lang.setOnClickListener(this);
        rel_feed=(RelativeLayout)findViewById(R.id.rel_feed);
        smartcrop=(TextView)findViewById(R.id.smartcrop);
        rel_feed.setOnClickListener(this);

        back=(ImageView)findViewById(R.id.menu);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(SellPost.this,BuySellActivity.class);
//                startActivity(intent);
                finish();
            }
        });
        smartcrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingScrren.this, HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rel_help:
                Intent rel_help=new Intent(SettingScrren.this,Help.class);
                startActivity(rel_help);
                break;
            case R.id.rel_account:
                Intent rel_account=new Intent(SettingScrren.this,DeleteAccount.class);
                startActivity(rel_account);
                break;
            case R.id.rel_wish:
                Intent rel_num=new Intent(SettingScrren.this,MywishList.class);
                startActivity(rel_num);
                finish();
                break;
            case R.id.rel_lang:
                Intent rel_email=new Intent(SettingScrren.this,Language.class);
                startActivity(rel_email);
                finish();
                break;
            case R.id.rel_noti:
                Intent rel_noti=new Intent(SettingScrren.this,MyAccount.class);
                startActivity(rel_noti);
                finish();

                break;
            case R.id.rel_feed:
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"umaiddhaliwal20@gmail.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(SettingScrren.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
                break;


        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(SettingScrren.this, HomeCategory.class);
//        startActivity(intent);
        finish();

    }
}
