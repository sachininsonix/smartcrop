package com.smartcrop;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by insonix on 24/12/15.
 */
public class Mysell extends Activity {
    String url,url2,id;
    SellListAdapter sellListAdapter;
    ImageLoader imageLoader;
   Button sellpost,buypost;
    SharedPreferences preferences;
    TextView smartcrop;
    DisplayImageOptions options;
    ImageView back,setting;
    ProgressDialog progressDialog;
    ListView mybuy;
    ArrayList<HashMap<String,String >> addme;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mysell);
        progressDialog=new ProgressDialog(Mysell.this);
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        imageLoader=ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(Mysell.this));
        options=new DisplayImageOptions.Builder().showStubImage(R.drawable.images).showImageForEmptyUri(R.drawable.profile_image).cacheOnDisc().cacheInMemory().build();
        back=(ImageView)findViewById(R.id.menu);
        setting=(ImageView)findViewById(R.id.setting);
        mybuy=(ListView)findViewById(R.id.mybuy);
        buypost=(Button)findViewById(R.id.buypost);
        sellpost=(Button)findViewById(R.id.sellpost);
        smartcrop=(TextView)findViewById(R.id.smartcrop);
        addme=new ArrayList<>();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Mysell.this, MyAccount.class);
                startActivity(intent);
                finish();
            }
        });
        sellpost.setBackgroundColor(Color.parseColor("#cc9516"));
        buypost.setBackgroundColor(Color.parseColor("#b2b2b2"));
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Mysell.this, SettingScrren.class);
                startActivity(intent);
//                finish();
            }
        });
        smartcrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Mysell.this, HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });
        buypost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Mysell.this, Mybuy.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
            }
        });
        mybuy.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(Mysell.this, SingleCategory.class);
                intent.putExtra("hashme", addme);
                intent.putExtra("position", position);
                intent.putExtra("back", "mysell");

                startActivity(intent);
            }
        });
        SellList();
    }
    public void SellList(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url="http://52.35.22.61/smart_crop/curent_user_sell.php?id="+preferences.getString("uid","");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("Status");
                    if(status.equalsIgnoreCase("OK")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("crop");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject City = jsonArray.getJSONObject(i);
                            String s_id = City.getString("id");
                            String id = City.getString("user_contact_id");
                            String subcategory_name = City.getString("subcategory_name");
                            String item = City.getString("category_name");
                            String Quantity = City.getString("quantity");
                            String Price = City.getString("price");
                            String Name = City.getString("name");
                            String State = City.getString("state");
                            String Description = City.getString("description");
                            String phone_number = City.getString("phone");
                            String profile_file = City.getString("profile_file");
                            String address = City.getString("address");
                            String district = City.getString("district");
                            String subcategory_id = City.getString("subcategory_id");
                            String min = City.getString("min");
                            String max = City.getString("max");//
                            String category_name = City.getString("category_name");
                            String categoryimage = City.getString("category_image");
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("id", id);
                            hashMap.put("sid", s_id);
                            hashMap.put("category_name", category_name);//
                            hashMap.put("subcategory_id", subcategory_id);
                            hashMap.put("Name", Name);
                            hashMap.put("item", item);
                            hashMap.put("Quantity", Quantity);
                            hashMap.put("subcategory_name", subcategory_name);
                            hashMap.put("Price", Price);
                            hashMap.put("city_name", State);
                            hashMap.put("profile_file", profile_file);
                            hashMap.put("Description", Description);
                            hashMap.put("state", State);
                            hashMap.put("phone_number", phone_number);
                          hashMap.put("Address", address);
                            hashMap.put("district", district);
                            hashMap.put("min", min);
                            hashMap.put("max", max);
                            hashMap.put("categoryimage", categoryimage);
                            addme.add(hashMap);
                        }
                        sellListAdapter=new SellListAdapter(Mysell.this, addme);
                        mybuy.setAdapter(sellListAdapter);//
                    }else{
                        new ShowMsg().createDialog(Mysell.this,"Oops! You have not Post yet.");
                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class SellListAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> addme;
        public SellListAdapter(Activity activity,ArrayList<HashMap<String,String>>addme){
            this.activity=activity;
            this.addme=addme;
        }
        @Override
        public int getCount() {
            return addme.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.mysell_items,null);

            TextView name=(TextView)convertView.findViewById(R.id.name);
            TextView username=(TextView)convertView.findViewById(R.id.user_name);
            TextView kilo=(TextView)convertView.findViewById(R.id.kilo);
            TextView price=(TextView)convertView.findViewById(R.id.price);
//            TextView phone=(TextView)convertView.findViewById(R.id.phone);
            ImageView edit=(ImageView)convertView.findViewById(R.id.edit);
            ImageView sell_image=(ImageView)convertView.findViewById(R.id.sell_image);//
            ImageView cross=(ImageView)convertView.findViewById(R.id.cross);
//           cross.setTag(position);
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(Mysell.this,EditSellPost.class);
                    intent.putExtra("hashme",addme);
                    intent.putExtra("position",position);
                    activity.startActivity(intent);
                    finish();
                }
            });
            cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    id=addme.get(position).get("sid");
                    AlertDialog.Builder alert = new AlertDialog.Builder(Mysell.this);

                    alert.setTitle("Are you sure you want to delete this post?");


                    alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            addme.remove(position);
                            sellListAdapter.notifyDataSetChanged();
                            Deletewish();


                        }
                    });

                    alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            // Canceled.
                            dialog.dismiss();

                        }
                    });


                    alert.show();

                }
            });
            name.setText(addme.get(position).get("item"));
            username.setText(addme.get(position).get("Name"));
            kilo.setText(addme.get(position).get("Quantity"));
            price.setText("RS: "+addme.get(position).get("min")+"-"+addme.get(position).get("max"));
//            phone.setText(addme.get(position).get("phone_number"));
            imageLoader.displayImage(addme.get(position).get("profile_file"), sell_image, options);
//

            return convertView;
        }
    }
    public void Deletewish(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url2="http://52.35.22.61/smart_crop/delete_sellpost.php?id="+id;
        Log.d("url2","url2"+url2);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url2, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("Status");
                    if(status.equalsIgnoreCase("OK")) {
//                        wishlist.setAdapter(sellListAdapter);
                        Toast.makeText(Mysell.this, "Successfully deleted", Toast.LENGTH_SHORT).show();


                    }else{
                        new ShowMsg().createDialog(Mysell.this,"Not deleted");
                    }


                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Mysell.this, MyAccount.class);
        startActivity(intent);
        finish();
    }
}