package com.smartcrop;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by insonix on 30/12/15.
 */
public class BuyFilters extends Activity {

    EditText quantity, price,maxprice,minprice;
    RelativeLayout sell_city, category_lay, filter_lay, sell_lay;
    ImageView back, setting,filter;
    ListView sell_list;
    ImageView add,sell;
    Button buy;
    InputMethodManager imm;
    Spinner spin_qua,spin_pri;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    ProgressDialog progressDialog;
    String url, lang, url1, subid, url2, quan_text,pri_text, cat_name, sub_name;
    int maxi,mini;
    ListView category_show, city_list;
    Button show;
    TextView category, city;
    ArrayList<HashMap<String, String>> addme;
    ArrayList<HashMap<String, String>> selllist;
    ArrayList<HashMap<String, String>> state_data;
    ImageLoader imageLoader;
    TextView set;
    DisplayImageOptions options;
    String[]quan={"/kg","/gm","/ton"};
    String[]price_spi={"kg","gm","ton"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filters);
        progressDialog = new ProgressDialog(BuyFilters.this);
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        addme = new ArrayList<>();
        selllist = new ArrayList<>();
        state_data = new ArrayList<>();
        spin_qua=(Spinner)findViewById(R.id.spin_qua);
        spin_pri=(Spinner)findViewById(R.id.spin_pri);
        set = (TextView) findViewById(R.id.set);
        sell=(ImageView)findViewById(R.id.sell);
        buy=(Button)findViewById(R.id.buy);
        add = (ImageView) findViewById(R.id.add);
        filter = (ImageView) findViewById(R.id.filter);
        sell_list = (ListView) findViewById(R.id.sell_list);
        city_list = (ListView) findViewById(R.id.city_list);
        category_show = (ListView) findViewById(R.id.category_show);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(BuyFilters.this));
        options = new DisplayImageOptions.Builder().showStubImage(R.drawable.images).showImageForEmptyUri(R.drawable.profile_image).cacheOnDisc().cacheInMemory().build();
        show = (Button) findViewById(R.id.show);
        category = (TextView) findViewById(R.id.category);
        sell_city = (RelativeLayout) findViewById(R.id.sell_city);
        category_lay = (RelativeLayout) findViewById(R.id.category_lay);
        filter_lay = (RelativeLayout) findViewById(R.id.filter_lay);
        sell_lay = (RelativeLayout) findViewById(R.id.sell_lay);
//        price = (EditText) findViewById(R.id.price);
        city = (TextView) findViewById(R.id.city);
        quantity = (EditText) findViewById(R.id.quantity);
        back = (ImageView) findViewById(R.id.menu);
        setting = (ImageView) findViewById(R.id.setting);
        maxprice=(EditText)findViewById(R.id.maxprice);
        minprice=(EditText)findViewById(R.id.minprice);
        addme = new ArrayList<>();
        try {
            subid = getIntent().getExtras().getString("subcat_id");
            cat_name = getIntent().getExtras().getString("catname");
            sub_name = getIntent().getExtras().getString("sub_cat_name");
//            total = getIntent().getExtras().getDouble("total");
//            Log.d("total:", "total" + total);
        } catch (Exception e) {

        }
//        set.setText("List of Buyers");

//        sell.setBackgroundColor(Color.parseColor("#b27c00"));
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuyFilters.this, SettingScrren.class);
                startActivity(intent);
//                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(BuyFilters.this, BuyList.class);
                intent.putExtra("subcat_id",subid);
                intent.putExtra("catname",cat_name);
                intent.putExtra("sub_cat_name", sub_name);
                startActivity(intent);
                finish();
            }
        });
        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuyFilters.this, SellingList.class);
                intent.putExtra("subcat_id", subid);
                intent.putExtra("catname", cat_name);
                intent.putExtra("sub_cat_name", sub_name);

                startActivity(intent);
            }
        });
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(minprice.getWindowToken(), 0);
                try {
                    maxi = Integer.parseInt(maxprice.getText().toString().trim());
                    mini = Integer.parseInt(minprice.getText().toString().trim());
                } catch (Exception e) {

                }

                if ((category.getText().toString().length() > 0) || (city.getText().toString().length() > 0)) {
//                    if (maxi > mini) {
                        SellList();
//                    } else {
//                        new ShowMsg().createDialog(BuyFilters.this, "Maximum value should not be less than Minimum value ");
//                    }
//                                     Log.d("url", "url" + url);
                } else {
                    new ShowMsg().createDialog(BuyFilters.this, "Please Choose either Category or State ");
                }
            }
//                    }
        });
        sell_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(BuyFilters.this, SingleCategory.class);
                intent.putExtra("hashme", selllist);
                intent.putExtra("position", position);
                intent.putExtra("subcat_id", subid);

                startActivity(intent);
            }
        });
      final  ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,quan);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<String>adapter1=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,price_spi);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_qua.setAdapter(adapter1);
//        spin_pri.setAdapter(adapter);
        maxprice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if ((maxprice.getText().toString().length() > 0)) {
                    spin_pri.setAdapter(adapter);

                } else {
                    spin_pri.setAdapter(null);
//            spin_pri.setAdapter(adapter);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        spin_qua.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                quan_text = spin_qua.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spin_pri.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pri_text = spin_pri.getSelectedItem().toString();
                Log.d("pri_text", "pri_text" + pri_text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        AllCategoryList();
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuyFilters.this, BuyFilters.class);
                intent.putExtra("subcat_id", subid);
                intent.putExtra("catname", cat_name);
                intent.putExtra("sub_cat_name", sub_name);
                startActivity(intent);
                finish();
            }
        });
        category_show.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                category_show.setVisibility(View.GONE);


                category.setText(addme.get(position).get("catname"));

            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuyFilters.this, SellPost.class);
                intent.putExtra("subcat_id", subid);
                intent.putExtra("catname", cat_name);
                intent.putExtra("sub_cat_name", sub_name);
                startActivity(intent);
                finish();
            }
        });

        sell_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                scrollView.setEnabled(false);
                city_list.setVisibility(View.VISIBLE);
                city_list.setAdapter(new CityAdapter(BuyFilters.this, state_data));
            }
        });
        category_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                scrollView.setEnabled(false);
                category_show.setVisibility(View.VISIBLE);
                category_show.setAdapter(new CategoryAdapter(BuyFilters.this, addme));
            }
        });

        CityList();
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuyFilters.this, BuyFilters.class);
                intent.putExtra("subcat_id", subid);
                intent.putExtra("catname", cat_name);
                intent.putExtra("sub_cat_name", sub_name);
                startActivity(intent);
                finish();
            }
        });

//
    }

    public void SellList() {
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
//        url2 = " http://52.35.22.61/smart_crop/filter_buy.php?category="+URLEncoder.encode(category.getText().toString()) + "&price="+price.getText().toString() + "&quantity="+quantity.getText().toString() + "&state=" + URLEncoder.encode(city.getText().toString()) + "&sub_category_id=" + subid + "&user_id=" + preferences.getString("uid", "");
        url2=" http://52.35.22.61/smart_crop/filter_buy.php?category="+URLEncoder.encode(category.getText().toString())+"&min="+minprice.getText().toString()+"&max="+maxprice.getText().toString()+""+pri_text+"&quantity="+quantity.getText().toString()+"&state="+URLEncoder.encode(city.getText().toString())+"&user_id="+preferences.getString("uid","");
        Log.d("url2:", "url2" + url2);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url2, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("status");
                    if(status.equalsIgnoreCase("OK")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("users_sell");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject City = jsonArray.getJSONObject(i);
                            String id = City.getString("User_Contact_ID");
                            String Name = City.getString("Name");
                            String item = City.getString("item");
                            String Quantity = City.getString("Quantity");
                            String min = City.getString("min");
                            String max = City.getString("max");
                            String City_na = City.getString("city_name");
                            String Description = City.getString("Description");
                            String phone_number = City.getString("phone_number");
                            String chat_id = City.getString("chat_id");
                            String profile_image = City.getString("profile_file");
                            String total = City.getString("total");
                            Log.d("CatName", "CatName" + Name);
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("id", id);
                            hashMap.put("Name", Name);
                            hashMap.put("item", item);
                            hashMap.put("Quantity", Quantity);
                            hashMap.put("min", min);
                            hashMap.put("Price","" );
                            hashMap.put("max", max);
                            hashMap.put("city_name", City_na);
                            hashMap.put("Description", Description);
                            hashMap.put("phone_number", phone_number);
                            hashMap.put("chat_id", chat_id);
                            hashMap.put("profile_file", profile_image);
                            hashMap.put("total", total);

                            selllist.add(hashMap);
                        }
                        sell_lay.setVisibility(View.VISIBLE);
                        filter_lay.setVisibility(View.GONE);
                        filter.setVisibility(View.VISIBLE);
                        sell_list.setAdapter(new SellListAdapter(BuyFilters.this, selllist));
                    } else {
//                        new ShowMsg().createDialog(BuyFilters.this, "Oops! no buyer in your area Be the first?");
                        //Put up the Yes/No message box
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(BuyFilters.this);
                        alertDialog.setTitle("              SmartCrop");
                        alertDialog.setMessage("Oops! no bouyer in your area Be the first?");
//                        alertDialog.setIcon(R.drawable.add_icon);


                        alertDialog.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Write your code here to execute after dialog
                                        Intent intent = new Intent(BuyFilters.this, Buy_post.class);
                                        intent.putExtra("subcat_id", subid);
                                        intent.putExtra("catname", cat_name);
                                        intent.putExtra("sub_cat_name", sub_name);
                                        startActivity(intent);
                                        overridePendingTransition(0, 0);
                                        finish();
//                                        Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
                                        // tv.setText("Yes Button clicked");
                                    }
                                });
                        alertDialog.setNegativeButton("NO",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        //  Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                                        dialog.cancel();
                                        //  tv.setText("No Button clicked");
                                    }
                                });

                        alertDialog.show();
                    }


                } catch (Exception e) {
                    Log.d("eeee:", "eee" + e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:", "volleyError" + volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }

    public void AllCategoryList() {

        progressDialog.setCancelable(false);
        progressDialog.show();
        url = "http://52.35.22.61/smart_crop/get_all_categories.php?city_id=" + URLEncoder.encode(preferences.getString("cname", "")) + "&lang=" + lang;
        Log.d("urlurlurl", "url" + url);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try {
                    String status = jsonObject.getString("Status");
                    if (status.equalsIgnoreCase("OK")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("category");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject catlist = jsonArray.getJSONObject(i);
                            String id = catlist.getString("ID");
                            String CatName = catlist.getString("CatName");

                            Log.d("CatName", "CatName" + CatName);
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("catname", CatName);

                            hashMap.put("catid", id);
                            addme.add(hashMap);
                        }

                        category_show.setAdapter(new CategoryAdapter(BuyFilters.this, addme));

                    } else {
                        new ShowMsg().createDialog(BuyFilters.this, "No record found");

                    }


                } catch (Exception e) {
                    Log.d("eeee:", "eee" + e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:", "volleyError" + volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }

    class CategoryAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String, String>> addme;

        public CategoryAdapter(Activity activity, ArrayList<HashMap<String, String>> addme) {
            this.activity = activity;
            this.addme = addme;
        }

        @Override
        public int getCount() {
            return addme.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.city_item, null);
//            ImageView cat_image=(ImageView)convertView.findViewById(R.id.cat_image);
            TextView cat_name = (TextView) convertView.findViewById(R.id.city_name);
            RelativeLayout rel_help = (RelativeLayout) convertView.findViewById(R.id.rel_help);
            cat_name.setText(addme.get(position).get("catname"));
//

            return convertView;
        }
    }

    public void CityList() {
//        progressDialog.setMessage("Loading...");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
        url1 = "http://52.35.22.61/smart_crop/get_all_cities.php";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try {
                    String status = jsonObject.getString("Status");

                    JSONArray jsonArray = jsonObject.getJSONArray("City");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject City = jsonArray.getJSONObject(i);
                        String id = City.getString("ID");
                        String City_Name = City.getString("City_Name");

                        Log.d("CatName", "CatName" + City_Name);
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put("City_Name", City_Name);
                        hashMap.put("City_id", id);

                        state_data.add(hashMap);
                    }
                    city_list.setAdapter(new CityAdapter(BuyFilters.this, state_data));


                } catch (Exception e) {
                    Log.d("eeee:", "eee" + e);
                }
//                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:", "volleyError" + volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }

    class CityAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String, String>> state_data;

        public CityAdapter(Activity activity, ArrayList<HashMap<String, String>> state_data) {
            this.activity = activity;
            this.state_data = state_data;
        }

        @Override
        public int getCount() {
            return state_data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.city_item, null);

            TextView city_name = (TextView) convertView.findViewById(R.id.city_name);
            city_name.setText(state_data.get(position).get("City_Name"));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    city_list.setVisibility(View.GONE);
                    city.setText(state_data.get(position).get("City_Name"));

                }
            });

            return convertView;
        }
    }

    class SellListAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String, String>> selllist;

        public SellListAdapter(Activity activity, ArrayList<HashMap<String, String>> selllist) {
            this.activity = activity;
            this.selllist = selllist;
        }

        @Override
        public int getCount() {
            return selllist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.selling_item, null);

            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView username = (TextView) convertView.findViewById(R.id.user_name);
            TextView kilo = (TextView) convertView.findViewById(R.id.kilo);
            TextView price = (TextView) convertView.findViewById(R.id.price);
            TextView phone = (TextView) convertView.findViewById(R.id.phone);
            TextView total_price = (TextView) convertView.findViewById(R.id.total_price);

            ImageView chat = (ImageView) convertView.findViewById(R.id.chat);
            ImageView sell_image = (ImageView) convertView.findViewById(R.id.sell_image);

            ImageView online = (ImageView) convertView.findViewById(R.id.online);
            TextView on = (TextView) convertView.findViewById(R.id.on);

            chat.setTag(position);
            if (preferences.getString("uid", "").equals(selllist.get(position).get("id"))) {
                chat.setVisibility(View.GONE);
            } else {
                chat.setVisibility(View.VISIBLE);
            }
            if (preferences.getString("Status", "").equals("1")) {
                chat.setBackgroundResource(R.drawable.chaton);
                online.setVisibility(View.VISIBLE);
                on.setVisibility(View.VISIBLE);
            } else {
                online.setVisibility(View.GONE);
                on.setVisibility(View.GONE);
                chat.setBackgroundResource(R.drawable.chatoff);
            }

            chat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id = (Integer) v.getTag();
                    Intent intent = new Intent(activity, ChatActivity.class);
                    intent.putExtra("chat_id", selllist.get(id).get("chat_id"));
                    intent.putExtra("id", selllist.get(id).get("id"));
//                    intent.putExtra("Name",addme.get(id).get("Name"));
//                    intent.putExtra("Price",addme.get(id).get("Price"));
//                    intent.putExtra("Price",addme.get(id).get("Price"));
                    activity.startActivity(intent);
                }
            });
            name.setText(selllist.get(position).get("item"));
            username.setText(selllist.get(position).get("Name"));
            kilo.setText(selllist.get(position).get("Quantity"));
            price.setText("RS: "+selllist.get(position).get("min")+"-"+selllist.get(position).get("max"));
            phone.setText(selllist.get(position).get("phone_number"));
            total_price.setText("Total RS: " + selllist.get(position).get("total"));

            imageLoader.displayImage(selllist.get(position).get("profile_file"), sell_image, options);
//

            return convertView;
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if(category_show.getVisibility()==View.VISIBLE ){
            category_show.setVisibility(View.GONE);
        }
        else if(city_list.getVisibility()==View.VISIBLE){
            city_list.setVisibility(View.GONE);
        } else{
            Intent intent = new Intent(BuyFilters.this, BuyList.class);
            intent.putExtra("subcat_id",subid);
            intent.putExtra("catname",cat_name);
            intent.putExtra("sub_cat_name", sub_name);
            startActivity(intent);
            finish();
        }
    }
}
