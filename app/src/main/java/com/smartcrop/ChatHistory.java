package com.smartcrop;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by insonix on 27/11/15.
 */
public class ChatHistory extends Activity {
    ListView chat_historylist;
    ImageView back,setting,nochat;
    String url;
    TextView smartcrop;
    SharedPreferences preferences;
    ArrayList<HashMap<String,String >> addme;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_chatlist);
        chat_historylist=(ListView)findViewById(R.id.chat_list);
        addme=new ArrayList<>();
        progressDialog=new ProgressDialog(ChatHistory.this);
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        back=(ImageView)findViewById(R.id.menu);
        nochat=(ImageView)findViewById(R.id.nochat);
        setting=(ImageView)findViewById(R.id.setting);
        smartcrop=(TextView)findViewById(R.id.smartcrop);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ChatHistory.this,HomeCategory.class);

                startActivity(intent);
                finish();
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChatHistory.this, SettingScrren.class);
                startActivity(intent);
//                finish();
            }
        });
        chat_historylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ChatHistory.this, ChatActivity.class);
                intent.putExtra("chat_id", addme.get(position).get("chat_id"));
                intent.putExtra("id", addme.get(position).get("r_id"));
                intent.putExtra("name", addme.get(position).get("name"));
                startActivity(intent);
            }
        });
        smartcrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChatHistory.this, HomeCategory.class);
                startActivity(intent);
                finish();
            }
        });
        ChathisList();
    }
    public void ChathisList(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url="http://52.35.22.61/smart_crop/get_all_chat_users.php?uid="+preferences.getString("uid","");

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("status");
                    if(status.equalsIgnoreCase("OK")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("users_sell");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject City = jsonArray.getJSONObject(i);
                            String chat_id = City.getString("chat_id");
                            String message = City.getString("message");
                            String r_id = City.getString("r_id");

                            String phone_number = City.getString("phone_number");
                            String name=City.getString("name");

                            HashMap<String, String> hashMap = new HashMap<>();

                            hashMap.put("message", message);
                            hashMap.put("r_id", r_id);

                            hashMap.put("phone_number", phone_number);
                            hashMap.put("chat_id", chat_id);
                            hashMap.put("name", name);

                            addme.add(hashMap);
                        }
                        chat_historylist.setAdapter(new ChathisAdapter(ChatHistory.this, addme));
                        nochat.setVisibility(View.GONE);
                    }else{
                        new ShowMsg().createDialog(ChatHistory.this,"No Chat history");
                        nochat.setVisibility(View.VISIBLE);
                        nochat.setBackgroundResource(R.drawable.chat_mai);
                    }

                }catch(Exception e){
                    Log.d("eeee:","eee"+e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class ChathisAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> addme;
        public ChathisAdapter(Activity activity,ArrayList<HashMap<String,String>>addme){
            this.activity=activity;
            this.addme=addme;
        }
        @Override
        public int getCount() {
            return addme.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.chat_historyitem,null);

            TextView message=(TextView)convertView.findViewById(R.id.message);

            TextView phone=(TextView)convertView.findViewById(R.id.phn_num);


            message.setText(addme.get(position).get("message"));
            if(addme.get(position).get("name").equals("")){
                phone.setText(addme.get(position).get("phone_number"));
            }else{
                phone.setText(addme.get(position).get("name"));
            }


//

            return convertView;
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(ChatHistory.this, Ho.class);
//        startActivity(intent);
        finish();
    }
}
